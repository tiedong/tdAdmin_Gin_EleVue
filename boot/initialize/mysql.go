package initialize

import (
	"fmt"
	"gorm.io/gorm/schema"
	"log"
	"os"
	"time"
	"vueBasic/config"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

func GetMysqlC(dsn string) *gorm.DB {
	newLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
		logger.Config{
			SlowThreshold: time.Second, // 慢 SQL 阈值
			LogLevel:      logger.Info, // Log level
			Colorful:      false,       // 禁用彩色打印
		},
	)
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{
		Logger: newLogger,
		// 外键约束
		DisableForeignKeyConstraintWhenMigrating: true,
		// 禁用默认事务（提高运行速度）
		SkipDefaultTransaction: true,
		NamingStrategy: schema.NamingStrategy{
			SingularTable: true,
		},
	})
	if err != nil {
		fmt.Println("连接数据库失败，请检查参数：", err)
		os.Exit(1)
	}
	sqlDB, _ := db.DB()
	sqlDB.SetMaxIdleConns(10)                  //设置连接池中的最大闲置连接数。
	sqlDB.SetMaxOpenConns(100)                 //设置数据库的最大连接数量
	sqlDB.SetConnMaxLifetime(10 * time.Second) //设置连接的最大可复用时间。
	return db

}

func GormMysqlByConfig(m config.Db) *gorm.DB {
	if m.Name == "" {
		return nil
	}
	dsn := m.Dsn()
	db := GetMysqlC(dsn)
	return db
}
