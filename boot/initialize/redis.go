package initialize

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"os"
	"vueBasic/global"
)

var redisContext = context.Background()

// 获取redis连接
func GetRedisC() *redis.Client {
	client := redis.NewClient(&redis.Options{
		Addr:     global.CONF.Cache.Addr,
		Password: global.CONF.Cache.Password, // no password set
		DB:       global.CONF.Cache.Db,       // use default DB
	})
	_, err := client.Ping(redisContext).Result()
	if err != nil {
		fmt.Println("redis连接失败")
		os.Exit(-1)
	}
	return client
}
