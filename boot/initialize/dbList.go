package initialize

import (
	"gorm.io/gorm"
	"vueBasic/config"
	"vueBasic/global"
)

// 把多个数据库入库
func DBList() map[string]*gorm.DB {
	dbMap := make(map[string]*gorm.DB)
	for _, info := range global.CONF.DbList {
		if info.Disable {
			continue
		}
		switch info.Type {
		case "mysql":
			dbMap[info.AliasName] = GormMysqlByConfig(config.Db{GeneralDB: info.GeneralDB})
		default:
			continue
		}
	}
	return dbMap
}
