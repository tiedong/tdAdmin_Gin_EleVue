package boot

import (
	"flag"
	"fmt"
	"github.com/fsnotify/fsnotify"
	"os"
	"vueBasic/boot/initialize"
	"vueBasic/extend/event"
	"vueBasic/extend/valid"
	"vueBasic/global"

	"github.com/spf13/viper"
)

// 启动执行
func init() {
	RootPath, _ := os.Getwd()
	v := viper.New()
	configName := flag.String("c", "config", "测试环境配置文件") //读取启动命令行参数获取 默认配置文件名 不填则默认config
	flag.Parse()
	v.SetConfigName(*configName)
	v.SetConfigType("yaml")
	v.AddConfigPath(RootPath)
	if err := v.ReadInConfig(); err != nil {
		panic(err)
	}

	//检测配置文件是否有修改过
	v.WatchConfig()
	v.OnConfigChange(func(e fsnotify.Event) {
		if err := v.Unmarshal(&global.CONF); err != nil {
			fmt.Println(err)
		}
	})
	if err := v.Unmarshal(&global.CONF); err != nil {
		fmt.Println(err)
	}
	//设置日志全局变量
	global.LOG = initialize.GetLogClient()
	//设置链接数据库
	global.DB = initialize.GetMysqlC(global.CONF.Db.Dsn())

	//数据多个数据库连接
	global.GVA_DBLIST = initialize.DBList()

	//访问日志全局对象
	if global.CONF.System.AccessLogStatus {
		global.ACCESSLOG = initialize.GetAccessLogClient()
	}
	//redis全局链接
	if global.CONF.Cache.Driver == "redis" {
		global.REDISDb = initialize.GetRedisC()
	}

	//注册验证器
	valid.InitValidator()
	//注册事件
	event.RegisterEvent()
}
