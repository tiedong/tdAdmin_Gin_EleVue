package middleware

import (
	"github.com/gin-gonic/gin"
	"time"
	"vueBasic/app/common/business"
	"vueBasic/extend/cache"
	"vueBasic/extend/conv"
)

// 获取系统配置写入上下文
func BeforeAction() gin.HandlerFunc {
	return func(c *gin.Context) {
		data, _ := cache.New().Remember("base_config", 30*time.Second, func() (string, error) {
			config, err := business.BaseConfigB.GetInfo()
			if err != nil {
				panic(err)
			}

			return conv.ObjectToString(config), nil
		})
		for k, v := range conv.StringToObject(data) {
			c.Set(k, v)
		}
		c.Next()
	}
}
