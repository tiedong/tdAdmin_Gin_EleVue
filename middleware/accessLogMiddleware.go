package middleware

import (
	"bytes"
	"github.com/gin-gonic/gin"
	"time"
	"vueBasic/extend/conv"
	"vueBasic/extend/utils"
	"vueBasic/global"
)

type AccessLogWriter struct {
	gin.ResponseWriter
	body *bytes.Buffer
}

func (w AccessLogWriter) Write(p []byte) (int, error) {
	if n, err := w.body.Write(p); err != nil {
		return n, err
	}
	return w.ResponseWriter.Write(p)
}

// 访问日志中间件
func AccessLog() gin.HandlerFunc {
	return func(c *gin.Context) {
		bodyWriter := &AccessLogWriter{body: bytes.NewBufferString(""), ResponseWriter: c.Writer}
		c.Writer = bodyWriter

		beginTime := time.Now().UnixNano()

		c.Next()

		endTime := time.Now().UnixNano()
		duration := endTime - beginTime

		global.ACCESSLOG.Infof("%s %s %s %s %s %d %d %d %s",
			utils.GetClientIP(c),
			conv.NowDateTime(),
			c.Request.Method,
			c.Request.RequestURI,
			c.Request.Proto,
			bodyWriter.Status(),
			bodyWriter.body.Len(),
			duration/1000,
			c.Request.Header.Get("User-Agent"),
		)
	}
}
