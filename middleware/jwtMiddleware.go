package middleware

import (
	"github.com/gin-gonic/gin"
	"vueBasic/extend/conv"
	e "vueBasic/extend/exception"
	"vueBasic/extend/jwt"
	"vueBasic/extend/utils"
)

// 解析jwt
func JwtVerify(app string) gin.HandlerFunc {
	return func(c *gin.Context) {
		//token := c.Request.Header.Get("Authorization")
		token := utils.GetSession("userToken", c)
		if token == "" {
			panic(e.ValidateError("token不能为空"))
			c.Redirect(302, "/admin/login/index")
		}
		data, err := jwt.NewJWT(app).ParseToken(conv.String(token))
		if err != nil {
			if err == jwt.TokenExpired {
				panic(e.ValidateWithCodeError{Msg: "token过期", Code: 101})
			} else {
				panic(e.ValidateWithCodeError{Msg: "token解析错误", Code: 102})
			}
			c.Redirect(302, "/admin/login/index")
		}
		c.Set("claims", data)
		c.Next()
	}
}
