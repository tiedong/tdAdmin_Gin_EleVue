package middleware

import (
	"github.com/gin-gonic/gin"
)

func AuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		//uri := c.Request.URL.Path
		//if conv.IsValueInList(uri, global.CONF.System.GetNotAuthUri()) {
		//	return
		//}
		//access := conv.StringToSlice(conv.String(utils.GetClaims("Access", c)), ",")
		//if conv.Int(utils.GetClaims("RoleId", c)) != 1 && !conv.IsValueInList(uri, access) {
		//	response.Fail(c, "没有权限访问")
		//	c.Abort()
		//	return
		//}
		c.Next()
	}
}
