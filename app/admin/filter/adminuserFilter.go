package filter

import (
	"vueBasic/app/common/request"
)

type AdminAdminuserReq struct {
	request.PageReq
	Username string      `form:"username" json:"username"` //用户名
	Nickname string      `form:"nickname" json:"nickname"` //昵称
	Mobile   string      `form:"mobile" json:"mobile" `    //手机
	RoleId   interface{} `form:"role_id" json:"role_id"`   //角色
	Status   interface{} `form:"status" json:"status"`     //状态 开启-1;关闭-0
}

/**
 * @description(数据列表)
 * @buildcode(true)
 */
type AdminAdminuserListRes struct {
	Id            uint   `json:"id"`              //主键id
	Username      string `json:"username"`        //用户名
	Nickname      string `json:"nickname"`        //昵称
	Status        int    `json:"status"`          //状态 开启-1;关闭-0
	Mobile        string `json:"mobile"`          //手机
	Email         string `json:"email"`           //邮箱
	Qq            string `json:"qq"`              //qq
	RoleName      string `json:"role_name"`       //角色id
	LastLoginTime string `json:"last_login_time"` //最后登录时间
	AddTime       string `json:"add_time"`        //创建时间
}

/**
 * @description(添加)
 * @buildcode(false)
 */
type AdminAdminuserAddReq struct {
	Username string `form:"username" json:"username" validate:"required" label:"用户名"` //角色名称
	Nickname string `form:"nickname" json:"nickname" `                                //昵称
	Password string `form:"password" json:"password" `                                //密码
	RoleId   int    `form:"role_id" json:"role_id"`                                   //角色
	Mobile   string `form:"mobile" json:"mobile" `                                    //手机
	Email    string `form:"email" json:"email" `                                      //邮箱
	Qq       string `form:"qq" json:"qq" `                                            //qq
	Status   int    `form:"status" json:"status" `                                    //状态
	AddTime  int    `form:"add_time" json:"add_time"`                                 //创建时间
}

/**
 * @description(获取修改方法详情)
 * @buildcode(true)
 */
type AdminAdminuserGetUpdateInfoReq struct {
	Id interface{} `form:"id" json:"id" validate:"required" label:"编号"` //主键id
}

/**
 * @description(修改)
 * @buildcode(false)
 */
type AdminAdminuserUpdateReq struct {
	Id uint `form:"id" json:"id" validate:"required" label:"编号"` //主键id
	AdminAdminuserAddReq
}

/**
 * @description(删除)
 * @buildcode(true)
 */
type AdminAdminuserDeleteReq struct {
	Id interface{} `form:"id" json:"id" validate:"required" label:"编号"` //主键id
}

/**
 * @description(重置密码)
 * @buildcode(true)
 */
type AdminAdminuserResetPwdReq struct {
	Id  interface{} `form:"id" json:"id" validate:"required" label:"编号"` //主键id
	Pwd string      `form:"pwd" json:"pwd"`                              //密码
}
