package filter

import "vueBasic/app/common/request"

// 用户登录
type AdminLoginReq struct {
	request.CaptchaReq
	User string `json:"username" validate:"required" label:"用户名"` //用户名
	Pwd  string `json:"password" validate:"required" label:"密码"`  //密码
}
