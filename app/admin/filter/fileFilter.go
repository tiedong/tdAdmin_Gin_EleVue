package filter

type AdminFileAddReq struct {
	Filepath   string `form:"filepath" json:"filepath"`       //图片路径
	Hash       string `form:"hash" json:"hash"`               //文件hash值
	CreateTime int64  `form:"create_time" json:"create_time"` //创建时间
	Disk       string `form:"disk" json:"disk"`               //存储方式
	Type       int8   `form:"type" json:"type"`               //文件类型
}

// 删除
type AdminFileDeleteReq struct {
	Filepath interface{} `json:"filepath" validate:"required" label:"文件地址"` //文件地址
}
