package filter

type DocReq struct {
	BookId interface{} `form:"book_id" json:"book_id" validate:"required" label:"编号"` //主键id
}

/**
 * @description(数据列表)
 * @buildcode(true)
 */
type DocNodeRes struct {
	Id       uint          `json:"val"`
	Title    string        `json:"key"`
	Type     int           `json:"type"`
	BookId   int           `json:"book_id"`
	Link     string        `json:"link"`
	Pid      int           `json:"pid"`
	Sort     int           `json:"sort"`
	Content  string        `json:"content"`
	Status   int           `json:"status"`
	Children []*DocNodeRes `json:"children"`
}

/**
 * @description(添加)
 * @buildcode(true)
 */
type DocAddReq struct {
	Title      string `form:"title" json:"title"` //标题
	Type       int    `form:"type" json:"type"`
	BookId     int    `form:"book_id" json:"book_id"`
	Pid        int    `form:"pid" json:"pid"`
	Sort       int    `form:"sort" json:"sort"`
	Status     int    `form:"status" json:"status"`
	AddTime    int64  `form:"add_time" json:"add_time"`
	UpdateTime int64  `form:"update_time" json:"update_time"`
}
type DocGetUpdateInfoReq struct {
	Id interface{} `form:"id" json:"id" validate:"required" label:"编号"` //主键id
}

/**
 * @description(更新)
 * @buildcode(true)
 */
type DocUpdateReq struct {
	Id         uint   `form:"id" json:"id"`
	Title      string `form:"title" json:"title"` //标题
	Type       int    `form:"type" json:"type"`
	BookId     int    `form:"book_id" json:"book_id"`
	Pid        int    `form:"pid" json:"pid"`
	Sort       int    `form:"sort" json:"sort"`
	Status     int    `form:"status" json:"status"`
	AddTime    int64  `form:"add_time" json:"add_time"`
	UpdateTime int64  `form:"update_time" json:"update_time"`
}
