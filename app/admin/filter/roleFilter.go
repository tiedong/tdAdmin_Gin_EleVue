package filter

import "vueBasic/app/common/request"

type AdminRoleReq struct {
	request.PageReq
	Title  string `form:"title" json:"title"`   //角色名称
	Status *int   `form:"status" json:"status"` //状态 开启-1;关闭-0
}

/**
 * @description(数据列表)
 * @buildcode(true)
 */
type AdminRoleListRes struct {
	Id          int    `json:"id"`          //主键id
	Title       string `json:"title"`       //角色名称
	Status      int    `json:"status"`      //状态 开启-1;关闭-0
	Description string `json:"description"` //描述
}

/**
 * @description(添加)
 * @buildcode(false)
 */
type AdminRoleAddReq struct {
	Title       string `form:"title" json:"title" validate:"required" label:"角色名称"` //角色名称
	Status      int    `form:"status" json:"status" `                               //状态
	Description string `form:"description" json:"description" `                     //描述
	AddTime     string `form:"add_time" json:"add_time"`                            //创建时间
}

/**
 * @description(获取修改方法详情)
 * @buildcode(true)
 */
type AdminRoleGetUpdateInfoReq struct {
	Id interface{} `form:"id" json:"id" validate:"required" label:"编号"` //主键id
}

/**
 * @description(修改)
 * @buildcode(false)
 */
type AdminRoleUpdateReq struct {
	Id uint `form:"id" json:"id" validate:"required" label:"编号"` //主键id
	AdminRoleAddReq
}

/**
 * @description(删除)
 * @buildcode(true)
 */
type AdminRoleDeleteReq struct {
	Id interface{} `form:"id" json:"id" validate:"required" label:"编号"` //主键id
}

/**
 * @description(获取授权数据)
 * @buildcode(true)
 */
type AdminRoleAuthReq struct {
	Id     int           `form:"id" json:"id" validate:"required" label:"编号"` //主键id
	Access []interface{} `form:"access" json:"access"`                        //角色id
}

/**
 * @description(添加授权)
 * @buildcode(true)
 */
type AdminRoleAccessReq struct {
	RoleId   int `json:"role_id"`
	AccessId int `json:"access_id"`
}
