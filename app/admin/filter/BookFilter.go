package filter

import "vueBasic/app/common/request"

type BookReq struct {
	request.PageReq
	Title   string      `form:"title" json:"title"`       //标题
	AddTime []string    `form:"add_time" json:"add_time"` //添加时间
	Status  interface{} `form:"status" json:"status"`     //状态 开启-1;关闭-0
}

/**
 * @description(数据列表)
 * @buildcode(true)
 */
type BookListRes struct {
	Id         uint   `json:"id"`    //主键id
	Title      string `json:"title"` //标题
	Sort       int    `json:"sort"`
	Desc       string `json:"desc"`
	AddTime    string `json:"add_time"`
	UpdateTime string `json:"update_time"`
}
type BookAddReq struct {
	Title      string `form:"title" json:"title"` //标题
	Desc       string `form:"desc" json:"desc"`
	Status     int    `form:"status" json:"status"`
	Thumb      string `form:"thumb" json:"thumb"`
	IsHome     int    `form:"is_home" json:"is_home"`
	Sort       int    `form:"sort" json:"sort"`
	AddTime    int64  `form:"add_time" json:"add_time"`
	UpdateTime int64  `form:"update_time" json:"update_time"`
}

type BookGetUpdateInfoReq struct {
	Id interface{} `form:"id" json:"id" validate:"required" label:"编号"` //主键id
}

type BookUpdateReq struct {
	Id         uint   `form:"id" json:"id"`
	Title      string `form:"title" json:"title"` //标题
	Desc       string `form:"desc" json:"desc"`
	Status     int    `form:"status" json:"status"`
	Thumb      string `form:"thumb" json:"thumb"`
	IsHome     int    `form:"is_home" json:"is_home"`
	Sort       int    `form:"sort" json:"sort"`
	AddTime    int64  `form:"add_time" json:"add_time"`
	UpdateTime int64  `form:"update_time" json:"update_time"`
}

type BookDeleteReq struct {
	Id interface{} `form:"id" json:"id" validate:"required" label:"编号"` //主键id
}
