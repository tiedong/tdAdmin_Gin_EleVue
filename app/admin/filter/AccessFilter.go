package filter

import "vueBasic/app/common/request"

type AccessNode struct {
	Id          uint          `form:"id" json:"id"`
	ModuleName  string        `form:"module_name" json:"module_name"` //模块名称
	ActionName  string        `form:"action_name" json:"action_name"` //操作名称
	Type        int           `form:"type" json:"type"`               //节点类型 :  1、表示模块    2、表示菜单     3、操作
	Url         string        `form:"url" json:"url"`                 //路由跳转地址
	Icon        string        `form:"icon" json:"icon"`               //图标
	ModuleId    int           `form:"module_id" json:"parentId"`      //此module_id和当前模型的id关联       module_id= 0 表示模块
	Sort        int           `form:"sort" json:"sort"`
	Description string        `form:"description" json:"description"`
	Status      int           `form:"status" json:"status"`
	Children    []*AccessNode `form:"children" json:"children"`
}

/**
 * @description(数据列表)
 * @buildcode(true)
 */
type AdminAccessPageReq struct {
	request.PageReq
	Id *uint `form:"id" json:"id"` //主键id
}

/**
 * @description(查询所属父类列表)
 * @buildcode(true)
 */
type AdminAccessPid struct {
	Id       int               `json:"val"`
	Name     string            `json:"key"`
	Pid      int               `json:"pid"`
	Children []*AdminAccessPid `json:"children" gorm:"-"`
}

/**
 * @description(添加)
 * @buildcode(true)
 */
type AdminAccessAddReq struct {
	ModuleName  string `form:"module_name" json:"module_name"` //模块名称
	ActionName  string `form:"action_name" json:"action_name"` //操作名称
	Type        int    `form:"type" json:"type"`               //节点类型 :  1、表示模块    2、表示菜单     3、操作
	Url         string `form:"url" json:"url"`                 //路由跳转地址
	Icon        string `form:"icon" json:"icon"`               //图标
	ModuleId    int    `form:"module_id" json:"module_id"`     //此module_id和当前模型的id关联       module_id= 0 表示模块
	Sort        int    `form:"sort" json:"sort"`
	Description string `form:"description" json:"description"`
	Status      int    `form:"status" json:"status"`
	AddTime     int    `form:"add_time" json:"add_time"`
	UpdateTime  int    `form:"update_time" json:"update_time"`
}

/**
 * @description(获取修改方法详情)
 * @buildcode(true)
 */
type AdminAccessGetUpdateInfoReq struct {
	Id interface{} `form:"id" json:"id" validate:"required" label:"编号"` //主键id
}

/**
 * @description(修改)
 * @buildcode(false)
 */
type AdminAccessUpdateReq struct {
	Id uint `form:"id" json:"id" validate:"required" label:"编号"` //主键id
	AdminAccessAddReq
}

/**
 * @description(删除)
 * @buildcode(true)
 */
type AdminAccessDeleteReq struct {
	Id interface{} `form:"id" json:"id" validate:"required" label:"编号"` //主键id
}

/**
 * @description(获取菜单)
 * @buildcode(true)
 */
type AdminMenuReq struct {
	Type      int      `json:"type"` //节点类型 :  1、表示模块    2、表示菜单     3、操作
	Status    int      `json:"status"`
	AccessIds []string `json:"access_ids"`
}
