package filter

import "vueBasic/app/common/request"

type ArticleReq struct {
	request.PageReq
	Title         string      `form:"title" json:"title"`                     //标题
	ArticleCateId interface{} `form:"article_cate_id" json:"article_cate_id"` //分类
	AddTime       []string    `form:"add_time" json:"add_time"`               //添加时间
	Status        interface{} `form:"status" json:"status"`                   //状态 开启-1;关闭-0
}

/**
 * @description(数据列表)
 * @buildcode(true)
 */
type ArticleListRes struct {
	Id         uint   `json:"id"`    //主键id
	Title      string `json:"title"` //标题
	Pid        int    `json:"pid"`
	Sort       int    `json:"sort"`
	Keywords   string `json:"keywords"`
	Desc       string `json:"desc"`
	AddTime    string `json:"add_time"`
	UpdateTime string `json:"update_time"`
}
type ArticleAddReq struct {
	Title         string `form:"title" json:"title"` //标题
	Keywords      string `form:"keywords" json:"keywords"`
	Desc          string `form:"desc" json:"desc"`
	Status        int    `form:"status" json:"status"`
	Thumb         string `form:"thumb" json:"thumb"`
	Original      int    `form:"original" json:"original"`
	Origin        string `form:"origin" json:"origin"`
	OriginUrl     string `form:"origin_url" json:"origin_url"`
	Content       string `form:"content" json:"content"`
	Read          int    `form:"read" json:"read"`
	Type          int    `form:"type" json:"type"`
	IsHome        int    `form:"is_home" json:"is_home"`
	Sort          int    `form:"sort" json:"sort"`
	ArticleCateId int    `form:"article_cate_id" json:"article_cate_id"`
	AddTime       int64  `form:"add_time" json:"add_time"`
	UpdateTime    int64  `form:"update_time" json:"update_time"`
	DeleteTime    int64  `form:"delete_time" json:"delete_time"`
}

type ArticleGetUpdateInfoReq struct {
	Id interface{} `form:"id" json:"id" validate:"required" label:"编号"` //主键id
}

type ArticleUpdateReq struct {
	Id            uint   `form:"id" json:"id"`
	Title         string `form:"title" json:"title"` //标题
	Keywords      string `form:"keywords" json:"keywords"`
	Desc          string `form:"desc" json:"desc"`
	Status        int    `form:"status" json:"status"`
	Thumb         string `form:"thumb" json:"thumb"`
	Original      int    `form:"original" json:"original"`
	Origin        string `form:"origin" json:"origin"`
	OriginUrl     string `form:"origin_url" json:"origin_url"`
	Content       string `form:"content" json:"content"`
	Read          int    `form:"read" json:"read"`
	Type          int    `form:"type" json:"type"`
	IsHome        int    `form:"is_home" json:"is_home"`
	Sort          int    `form:"sort" json:"sort"`
	ArticleCateId int    `form:"article_cate_id" json:"article_cate_id"`
	AddTime       int64  `form:"add_time" json:"add_time"`
	UpdateTime    int64  `form:"update_time" json:"update_time"`
	DeleteTime    int64  `form:"delete_time" json:"delete_time"`
}

type ArticleDeleteReq struct {
	Id interface{} `form:"id" json:"id" validate:"required" label:"编号"` //主键id
}
