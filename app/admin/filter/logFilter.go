package filter

import "vueBasic/app/common/request"

type AdminLogListReq struct {
	request.PageReq
	Username   string      `form:"username" json:"username"`       //用户名
	Type       interface{} `form:"type" json:"type"`               //类型 :  1、登录日志    2、操作日志     3、异常日志
	CreateTime []string    `form:"create_time" json:"create_time"` //创建时间
}

type AdminLogListRes struct {
	Id int `json:"id"` //编号
	AdminLogAddReq
}

type AdminLogAddReq struct {
	ApplicationName string `json:"application_name"` //所属应用
	Type            int    `json:"type"`             //日志类型 登录日志-1;操作日志-2;异常日志-3
	Username        string `json:"username"`         //用户名
	Url             string `json:"url"`              //请求url
	Ip              string `json:"ip"`               //客户端ip
	Useragent       string `json:"useragent"`        //浏览器信息
	Content         string `json:"content"`          //请求内容
	Errmsg          string `json:"errmsg"`           //异常信息
	CreateTime      int64  `json:"create_time"`      //创建时间
}

/**
 * @description(删除)
 * @buildcode(true)
 */
type AdminLogDeleteReq struct {
	Id interface{} `form:"id" json:"id" validate:"required" label:"编号"` //主键id
}

/**
 * @description(详情)
 * @buildcode(true)
 */
type AdminLogInfoReq struct {
	Id interface{} `form:"id" json:"id" validate:"required" label:"编号"` //主键id
}
