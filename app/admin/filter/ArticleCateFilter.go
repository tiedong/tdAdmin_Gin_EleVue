package filter

import "vueBasic/app/common/request"

type ArticleCateReq struct {
	request.PageReq
}

/**
 * @description(数据列表)
 * @buildcode(true)
 */
type ArticleCateListRes struct {
	Id         uint   `json:"id"`    //主键id
	Title      string `json:"title"` //标题
	Pid        int    `json:"pid"`
	Sort       int    `json:"sort"`
	Keywords   string `json:"keywords"`
	Desc       string `json:"desc"`
	AddTime    string `json:"add_time"`
	UpdateTime string `json:"update_time"`
}
type ArticleCateAddReq struct {
	Id         uint   `form:"id" json:"id"`
	Title      string `form:"title" json:"title"` //标题
	Pid        int    `form:"pid" json:"pid"`
	Sort       int    `form:"sort" json:"sort"`
	Keywords   string `form:"keywords" json:"keywords"`
	Desc       string `form:"desc" json:"desc"`
	AddTime    int64  `form:"add_time" json:"add_time"`
	UpdateTime int64  `form:"update_time" json:"update_time"`
}

type ArticleCateGetUpdateInfoReq struct {
	Id interface{} `form:"id" json:"id" validate:"required" label:"编号"` //主键id
}

type ArticleCateUpdateReq struct {
	Id         uint   `form:"id" json:"id"`
	Title      string `form:"title" json:"title"` //标题
	Pid        int    `form:"pid" json:"pid"`
	Sort       int    `form:"sort" json:"sort"`
	Keywords   string `form:"keywords" json:"keywords"`
	Desc       string `form:"desc" json:"desc"`
	UpdateTime int64  `form:"update_time" json:"update_time"`
}

type ArticleCateDeleteReq struct {
	Id interface{} `form:"id" json:"id" validate:"required" label:"编号"` //主键id
}

/**
 * @description(查询所属父类列表)
 * @buildcode(true)
 */
type ArticleCatePid struct {
	Id       int               `json:"val"`
	Title    string            `json:"key"`
	Pid      int               `json:"pid"`
	Children []*ArticleCatePid `json:"children" gorm:"-"`
}
