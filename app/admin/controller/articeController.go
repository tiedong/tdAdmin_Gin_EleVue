package controller

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"vueBasic/app"
	"vueBasic/app/admin/filter"
	"vueBasic/app/common/business"
	"vueBasic/app/common/request"
	"vueBasic/app/common/response"
	"vueBasic/extend/conv"
	"vueBasic/extend/utils"
)

var ArticleC = new(articleController)

type articleController struct {
	app.BaseController
}

func (con *articleController) Index(c *gin.Context) {
	c.HTML(http.StatusOK, "admin/article/index.html", gin.H{
		"v": conv.NowUnix(),
	})
}

func (con *articleController) GetList(c *gin.Context) {
	req := filter.ArticleReq{
		PageReq: request.PageReq{
			Page:  1,
			Limit: 20,
		},
	}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	list, cnt, rErr := business.ArticleB.GetPageList(&req)
	if rErr != nil {
		response.Err(c, rErr)
		return
	}
	response.Success(c, "返回成功", gin.H{
		"data":  list,
		"total": cnt,
	})
}

func (con *articleController) Add(c *gin.Context) {
	var req *filter.ArticleAddReq
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	res, rErr := business.ArticleB.Add(req)
	if rErr != nil {
		response.Err(c, rErr)
		return
	}
	response.Success(c, "添加成功", res)
}

func (con *articleController) GetUpdateInfo(c *gin.Context) {
	var req *filter.ArticleCateGetUpdateInfoReq
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	info, rErr := business.ArticleB.GetUpdateInfo(req)
	if rErr != nil {
		response.Err(c, rErr)
		return
	}
	response.Success(c, "返回成功", info)
}

func (con *articleController) Update(c *gin.Context) {
	var req *filter.ArticleUpdateReq
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	res, rErr := business.ArticleB.Update(req)
	if rErr != nil {
		response.Err(c, rErr)
		return
	}
	response.Success(c, "更新成功", res)
}

func (con *articleController) UpdateExt(c *gin.Context) {
	var req map[string]interface{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	rErr := business.ArticleB.UpdateExt(req)
	if rErr != nil {
		response.Err(c, rErr)
		return
	}
	response.Success(c, "更新成功", nil)
}

func (con *articleController) Delete(c *gin.Context) {
	var req *filter.ArticleDeleteReq
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	res, rErr := business.ArticleB.Delete(req)
	if rErr != nil {
		response.Err(c, rErr)
		return
	}
	response.Success(c, "删除成功", res)
}
