package controller

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"vueBasic/app"
	"vueBasic/app/admin/filter"
	"vueBasic/app/common/business"
	"vueBasic/app/common/request"
	"vueBasic/app/common/response"
	"vueBasic/extend/conv"
	"vueBasic/extend/utils"
)

var ArticleCateC = new(articleCateController)

type articleCateController struct {
	app.BaseController
}

func (con *articleCateController) Index(c *gin.Context) {
	c.HTML(http.StatusOK, "admin/article_cate/index.html", gin.H{
		"v": conv.NowUnix(),
	})
}

func (con *articleCateController) GetList(c *gin.Context) {
	req := filter.ArticleCateReq{
		PageReq: request.PageReq{
			Page:  1,
			Limit: 20,
		},
	}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	list, cnt, rErr := business.ArticleCateB.GetPageList(&req)
	if rErr != nil {
		response.Err(c, rErr)
		return
	}
	response.Success(c, "返回成功", gin.H{
		"data":  list,
		"total": cnt,
	})
}

// 添加
func (con *articleCateController) Add(c *gin.Context) {
	var req *filter.ArticleCateAddReq
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	res, rErr := business.ArticleCateB.Add(req)
	if rErr != nil {
		response.Err(c, rErr)
		return
	}
	response.Success(c, "添加成功", res)
}

func (con *articleCateController) GetUpdateInfo(c *gin.Context) {
	var req *filter.ArticleCateGetUpdateInfoReq
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	info, rErr := business.ArticleCateB.GetUpdateInfo(req)
	if rErr != nil {
		response.Err(c, rErr)
		return
	}
	response.Success(c, "返回成功", info)
}

func (con *articleCateController) Update(c *gin.Context) {
	var req *filter.ArticleCateUpdateReq
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	res, rErr := business.ArticleCateB.Update(req)
	if rErr != nil {
		response.Err(c, rErr)
		return
	}
	response.Success(c, "更新成功", res)
}

func (con *articleCateController) UpdateExt(c *gin.Context) {
	var req map[string]interface{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	rErr := business.ArticleCateB.UpdateExt(req)
	if rErr != nil {
		response.Err(c, rErr)
		return
	}
	response.Success(c, "更新成功", nil)
}

func (con *articleCateController) Delete(c *gin.Context) {
	var req *filter.ArticleCateDeleteReq
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	res, rErr := business.ArticleCateB.Delete(req)
	if rErr != nil {
		response.Err(c, rErr)
		return
	}
	response.Success(c, "删除成功", res)
}

func (con *articleCateController) GetFieldList(c *gin.Context) {
	list, err := business.ArticleCateB.GetFieldList()
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "返回成功", list)
}
