package controller

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"vueBasic/app"
	"vueBasic/app/admin/filter"
	"vueBasic/app/common/business"
	"vueBasic/app/common/response"
	"vueBasic/extend/conv"
	"vueBasic/extend/utils"
)

var DocC = new(docController)

type docController struct {
	app.BaseController
}

func (con *docController) Index(c *gin.Context) {
	c.HTML(http.StatusOK, "admin/doc/index.html", gin.H{
		"v": conv.NowUnix(),
	})
}

func (con *docController) GetList(c *gin.Context) {
	var req *filter.DocReq
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	list, cnt, rErr := business.DocB.GetList(req)
	if rErr != nil {
		response.Err(c, rErr)
		return
	}
	response.Success(c, "返回成功", gin.H{
		"data":  list,
		"total": cnt,
	})
}

func (con *docController) Add(c *gin.Context) {
	var req *filter.DocAddReq
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	res, rErr := business.DocB.Add(req)
	if rErr != nil {
		response.Err(c, rErr)
		return
	}
	response.Success(c, "添加成功", res)
}

func (con *docController) GetUpdateInfo(c *gin.Context) {
	var req *filter.DocGetUpdateInfoReq
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	info, rErr := business.DocB.GetUpdateInfo(req)
	if rErr != nil {
		response.Err(c, rErr)
		return
	}
	response.Success(c, "返回成功", info)
}

func (con *docController) Update(c *gin.Context) {
	var req *filter.DocUpdateReq
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	res, rErr := business.DocB.Update(req)
	if rErr != nil {
		response.Err(c, rErr)
		return
	}
	response.Success(c, "更新成功", res)
}

func (con *docController) UpdateExt(c *gin.Context) {
	var req map[string]interface{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	rErr := business.DocB.UpdateExt(req)
	if rErr != nil {
		response.Err(c, rErr)
		return
	}
	response.Success(c, "更新成功", nil)
}

func (con *docController) Delete(c *gin.Context) {
	var req *filter.BookDeleteReq
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	res, rErr := business.BookB.Delete(req)
	if rErr != nil {
		response.Err(c, rErr)
		return
	}
	response.Success(c, "删除成功", res)
}
