package controller

import (
	"github.com/gin-gonic/gin"
	"vueBasic/app"
	"vueBasic/app/admin/filter"
	"vueBasic/app/common/business"
	"vueBasic/app/common/response"
	"vueBasic/extend/conv"
	"vueBasic/extend/upload"
	"vueBasic/extend/utils"
)

var BasicC = new(baseController)

type baseController struct {
	app.BaseController
}

func (con *baseController) GetMenu(c *gin.Context) {
	nickname := utils.GetClaims("Nickname", c)
	role_id := conv.Int(utils.GetClaims("RoleId", c))
	urlList := business.AccessB.GetRoleUrl(role_id)
	menu := business.BasicB.GetMenuList(role_id)
	response.Success(c, "返回成功", gin.H{
		"menu":     menu,
		"nickname": nickname,
		"url":      urlList,
		"role_id":  role_id,
	})
}

func (con *baseController) GetRoleMenus(c *gin.Context) {
	roleMenu := business.BasicB.GetRoleMenus()
	response.Success(c, "返回成功", roleMenu)
}

func (con *baseController) Upload(c *gin.Context) {
	rename := c.DefaultPostForm("rename", "1")
	file, err := c.FormFile("file")
	if err != nil {
		response.Fail(c, err.Error())
		return
	}
	res, rErr := upload.Upload(file, rename, c)
	if rErr != nil {
		response.Fail(c, rErr.Error())
		return
	}
	response.Success(c, "返回成功", res)
}

func (con *baseController) DeleteFile(c *gin.Context) {
	var req *filter.AdminFileDeleteReq
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	rErr := business.BasicB.DeleteFile(req)
	if rErr != nil {
		response.Fail(c, rErr.Error())
		return
	}
	response.Success(c, "返回成功", nil)
}
