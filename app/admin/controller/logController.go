package controller

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"vueBasic/app"
	"vueBasic/app/admin/filter"
	"vueBasic/app/common/business"
	"vueBasic/app/common/request"
	"vueBasic/app/common/response"
	"vueBasic/extend/conv"
	"vueBasic/extend/utils"
)

var LogC = new(logController)

type logController struct {
	app.BaseController
}

func (con *logController) Index(c *gin.Context) {
	c.HTML(http.StatusOK, "admin/log/index.html", gin.H{
		"v": conv.NowUnix(),
	})
}

// 获取角色列表数据
func (con *logController) GetList(c *gin.Context) {
	req := filter.AdminLogListReq{
		PageReq: request.PageReq{Page: 1, Limit: 20},
	}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	roleList, cnt, errRes := business.LogB.GetPageList(&req)
	if errRes != nil {
		response.Err(c, errRes)
		return
	}
	response.Success(c, "返回成功", gin.H{
		"data":  roleList,
		"total": cnt,
	})
}

// 详情
func (con *logController) Detail(c *gin.Context) {
	req := filter.AdminLogInfoReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	info, rErr := business.LogB.GetUpdateInfo(&req)
	if rErr != nil {
		response.Err(c, rErr)
		return
	}
	response.Success(c, "返回成功", info)
}

// 删除
func (con *logController) Delete(c *gin.Context) {
	req := filter.AdminLogDeleteReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	res, rErr := business.LogB.Delete(&req)
	if rErr != nil {
		response.Err(c, rErr)
		return
	}
	response.Success(c, "删除成功", res)
}

func (con *logController) Export(c *gin.Context) {
	response.Success(c, "返回成功", nil)
}
