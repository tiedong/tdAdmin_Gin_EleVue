package controller

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"vueBasic/app"
	"vueBasic/app/admin/filter"
	"vueBasic/app/common/business"
	"vueBasic/app/common/request"
	"vueBasic/app/common/response"
	"vueBasic/extend/conv"
	"vueBasic/extend/utils"
)

var BookC = new(bookController)

type bookController struct {
	app.BaseController
}

func (con *bookController) Index(c *gin.Context) {
	c.HTML(http.StatusOK, "admin/book/index.html", gin.H{
		"v": conv.NowUnix(),
	})
}

func (con *bookController) GetList(c *gin.Context) {
	req := filter.BookReq{
		PageReq: request.PageReq{
			Page:  1,
			Limit: 20,
		},
	}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	list, cnt, rErr := business.BookB.GetPageList(&req)
	if rErr != nil {
		response.Err(c, rErr)
		return
	}
	response.Success(c, "返回成功", gin.H{
		"data":  list,
		"total": cnt,
	})
}

func (con *bookController) Add(c *gin.Context) {
	var req *filter.BookAddReq
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	res, rErr := business.BookB.Add(req)
	if rErr != nil {
		response.Err(c, rErr)
		return
	}
	response.Success(c, "添加成功", res)
}

func (con *bookController) GetUpdateInfo(c *gin.Context) {
	var req *filter.BookGetUpdateInfoReq
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	info, rErr := business.BookB.GetUpdateInfo(req)
	if rErr != nil {
		response.Err(c, rErr)
		return
	}
	response.Success(c, "返回成功", info)
}

func (con *bookController) Update(c *gin.Context) {
	var req *filter.BookUpdateReq
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	res, rErr := business.BookB.Update(req)
	if rErr != nil {
		response.Err(c, rErr)
		return
	}
	response.Success(c, "更新成功", res)
}

func (con *bookController) UpdateExt(c *gin.Context) {
	var req map[string]interface{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	rErr := business.BookB.UpdateExt(req)
	if rErr != nil {
		response.Err(c, rErr)
		return
	}
	response.Success(c, "更新成功", nil)
}

func (con *bookController) Delete(c *gin.Context) {
	var req *filter.BookDeleteReq
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	res, rErr := business.BookB.Delete(req)
	if rErr != nil {
		response.Err(c, rErr)
		return
	}
	response.Success(c, "删除成功", res)
}
