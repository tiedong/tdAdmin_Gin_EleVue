package controller

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"vueBasic/app"
	"vueBasic/app/admin/filter"
	"vueBasic/app/common/business"
	"vueBasic/app/common/request"
	"vueBasic/app/common/response"
	"vueBasic/extend/conv"
	"vueBasic/extend/utils"
)

var AccessC = new(accessController)

type accessController struct {
	app.BaseController
}

func (con *accessController) Index(c *gin.Context) {
	c.HTML(http.StatusOK, "admin/access/index.html", gin.H{
		"v": conv.NowUnix(),
	})
}

// 获取菜单列表数据
func (con *accessController) GetList(c *gin.Context) {
	req := filter.AdminAccessPageReq{
		PageReq: request.PageReq{Page: 1, Limit: 100},
	}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	roleList, cnt, errRes := business.AccessB.GetPageList(&req)
	if errRes != nil {
		response.Err(c, errRes)
		return
	}
	response.Success(c, "返回成功", gin.H{
		"data":  roleList,
		"total": cnt,
	})
}

// 添加页面展示
func (con *accessController) Add(c *gin.Context) {
	req := filter.AdminAccessAddReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	res, err := business.AccessB.Add(&req)
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "添加成功", res)
}

// 获取详情
func (con *accessController) GetUpdateInfo(c *gin.Context) {
	req := filter.AdminAccessGetUpdateInfoReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	info, rErr := business.AccessB.GetUpdateInfo(&req)
	if rErr != nil {
		response.Err(c, rErr)
		return
	}
	response.Success(c, "返回成功", info)
}

// 更新
func (con *accessController) Update(c *gin.Context) {
	req := filter.AdminAccessUpdateReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	res, rErr := business.AccessB.Update(&req)
	if rErr != nil {
		response.Err(c, rErr)
		return
	}
	response.Success(c, "更新成功", res)
}

// 修改状态
func (con *accessController) UpdateExt(c *gin.Context) {
	var req map[string]interface{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	rErr := business.AccessB.UpdateExt(req)
	if rErr != nil {
		response.Err(c, rErr)
		return
	}
	response.Success(c, "更新成功", nil)
}

// 删除
func (con *accessController) Delete(c *gin.Context) {
	req := filter.AdminAccessDeleteReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	res, rErr := business.AccessB.Delete(&req)
	if rErr != nil {
		response.Err(c, rErr)
		return
	}
	response.Success(c, "删除成功", res)
}

// 详情
func (con *accessController) Detail(c *gin.Context) {
	req := filter.AdminAccessGetUpdateInfoReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	info, rErr := business.AccessB.GetUpdateInfo(&req)
	if rErr != nil {
		response.Err(c, rErr)
		return
	}
	response.Success(c, "返回成功", info)
}

func (con *accessController) GetFieldList(c *gin.Context) {
	list, err := business.AccessB.GetFieldList()
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "返回成功", list)
}
