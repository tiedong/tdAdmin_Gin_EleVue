package controller

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"vueBasic/app"
	"vueBasic/app/admin/filter"
	"vueBasic/app/common/business"
	"vueBasic/app/common/request"
	"vueBasic/app/common/response"
	"vueBasic/extend/conv"
	"vueBasic/extend/utils"
)

var RoleC = new(roleController)

type roleController struct {
	app.BaseController
}

func (con *roleController) Index(c *gin.Context) {
	c.HTML(http.StatusOK, "admin/role/index.html", gin.H{
		"v": conv.NowUnix(),
	})
}

// 获取角色列表数据
func (con *roleController) GetList(c *gin.Context) {
	req := filter.AdminRoleReq{
		PageReq: request.PageReq{Page: 1, Limit: 20},
	}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	roleList, cnt, errRes := business.RoleB.GetPageList(&req)
	if errRes != nil {
		response.Err(c, errRes)
		return
	}
	response.Success(c, "返回成功", gin.H{
		"data":  roleList,
		"total": cnt,
	})
}

// 添加页面展示
func (con *roleController) Add(c *gin.Context) {
	req := filter.AdminRoleAddReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	res, rErr := business.RoleB.Add(&req)
	if rErr != nil {
		response.Err(c, rErr)
		return
	}
	response.Success(c, "添加成功", res)
}

// 获取详情
func (con *roleController) GetUpdateInfo(c *gin.Context) {
	req := filter.AdminRoleGetUpdateInfoReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	info, rErr := business.RoleB.GetUpdateInfo(&req)
	if rErr != nil {
		response.Err(c, rErr)
		return
	}
	response.Success(c, "返回成功", info)
}

// 更新
func (con *roleController) Update(c *gin.Context) {
	req := filter.AdminRoleUpdateReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	res, rErr := business.RoleB.Update(&req)
	if rErr != nil {
		response.Err(c, rErr)
		return
	}
	response.Success(c, "更新成功", res)
}

// 修改状态
func (con *roleController) UpdateExt(c *gin.Context) {
	var req map[string]interface{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	rErr := business.RoleB.UpdateExt(req)
	if rErr != nil {
		response.Err(c, rErr)
		return
	}
	response.Success(c, "更新成功", nil)
}

// 删除
func (con *roleController) Delete(c *gin.Context) {
	req := filter.AdminRoleDeleteReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	res, rErr := business.RoleB.Delete(&req)
	if rErr != nil {
		response.Err(c, rErr)
		return
	}
	response.Success(c, "删除成功", res)
}

// 授权
func (con *roleController) Auth(c *gin.Context) {
	req := filter.AdminRoleAuthReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	rErr := business.RoleB.DoAuth(&req)
	if rErr != nil {
		response.Err(c, rErr)
		return
	}
	response.Success(c, "操作成功", rErr)
}
