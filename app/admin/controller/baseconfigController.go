package controller

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"vueBasic/app"
	"vueBasic/app/common/business"
	"vueBasic/app/common/response"
	"vueBasic/extend/conv"
)

var BasicConfigC = new(baseConfigController)

type baseConfigController struct {
	app.BaseController
}

func (con *baseConfigController) Index(c *gin.Context) {
	c.HTML(http.StatusOK, "admin/base_config/index.html", gin.H{
		"v": conv.NowUnix(),
	})
}

func (con *baseConfigController) GetInfo(c *gin.Context) {
	info, err := business.BaseConfigB.GetInfo()
	if err != nil {
		response.Err(c, err)
		return
	}
	response.Success(c, "返回成功", info)
}

func (con *baseConfigController) Update(c *gin.Context) {
	var req map[string]interface{}
	if err := c.ShouldBind(&req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	rErr := business.BaseConfigB.Update(req)
	if rErr != nil {
		response.Err(c, rErr)
		return
	}
	response.Success(c, "修改配置信息成功", rErr)
}
