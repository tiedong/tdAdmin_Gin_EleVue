package controller

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"vueBasic/app"
)

var IndexC = new(indexController)

type indexController struct {
	app.BaseController
}

func (con *indexController) Index(c *gin.Context) {
	c.HTML(http.StatusOK, "admin/index/index.html", gin.H{})
}

func (con *indexController) Main(c *gin.Context) {
	c.HTML(http.StatusOK, "admin/index/main.html", gin.H{})
}
