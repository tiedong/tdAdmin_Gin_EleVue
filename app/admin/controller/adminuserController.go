package controller

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"vueBasic/app"
	"vueBasic/app/admin/filter"
	"vueBasic/app/common/business"
	"vueBasic/app/common/request"
	"vueBasic/app/common/response"
	"vueBasic/extend/conv"
	"vueBasic/extend/utils"
)

var AdminUserC = new(adminuserController)

type adminuserController struct {
	app.BaseController
}

func (con *adminuserController) Index(c *gin.Context) {
	c.HTML(http.StatusOK, "admin/adminuser/index.html", gin.H{
		"v": conv.NowUnix(),
	})
}

// 获取角色列表数据
func (con *adminuserController) GetList(c *gin.Context) {
	req := filter.AdminAdminuserReq{
		PageReq: request.PageReq{Page: 1, Limit: 20},
	}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	userList, cnt, errRes := business.AdminuserB.GetPageList(&req)
	if errRes != nil {
		response.Err(c, errRes)
		return
	}
	roleData := business.AdminuserB.GetFieldList()
	response.Success(c, "返回成功", gin.H{
		"data":     userList,
		"role_ids": roleData,
		"total":    cnt,
	})
}

func (con *adminuserController) GetFieldList(c *gin.Context) {
	data := business.AdminuserB.GetFieldList()
	response.Success(c, "返回成功", gin.H{
		"role_ids": data,
	})
}

// 添加页面展示
func (con *adminuserController) Add(c *gin.Context) {
	req := filter.AdminAdminuserAddReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	res, rErr := business.AdminuserB.Add(&req)
	if rErr != nil {
		response.Err(c, rErr)
		return
	}
	response.Success(c, "添加成功", res)
}

// 获取详情
func (con *adminuserController) GetUpdateInfo(c *gin.Context) {
	req := filter.AdminAdminuserGetUpdateInfoReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	info, rErr := business.AdminuserB.GetUpdateInfo(&req)
	if rErr != nil {
		response.Err(c, rErr)
		return
	}
	response.Success(c, "返回成功", info)
}

// 更新
func (con *adminuserController) Update(c *gin.Context) {
	req := filter.AdminAdminuserUpdateReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	res, rErr := business.AdminuserB.Update(&req)
	if rErr != nil {
		response.Err(c, rErr)
		return
	}
	response.Success(c, "更新成功", res)
}

// 修改状态
func (con *adminuserController) UpdateExt(c *gin.Context) {
	var req map[string]interface{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	rErr := business.AdminuserB.UpdateExt(req)
	if rErr != nil {
		response.Err(c, rErr)
		return
	}
	response.Success(c, "更新成功", nil)
}

// 删除
func (con *adminuserController) Delete(c *gin.Context) {
	req := filter.AdminAdminuserDeleteReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	res, rErr := business.AdminuserB.Delete(&req)
	if rErr != nil {
		response.Err(c, rErr)
		return
	}
	response.Success(c, "删除成功", res)
}

// 重置密码
func (con *adminuserController) ResetPwd(c *gin.Context) {
	req := filter.AdminAdminuserResetPwdReq{}
	if err := utils.BindAndValid(c, &req); err != nil {
		response.Fail(c, err.Error())
		return
	}
	rErr := business.AdminuserB.ResetPwd(&req)
	if rErr != nil {
		response.Err(c, rErr)
		return
	}
	response.Success(c, "重置密码成功", rErr)
}
