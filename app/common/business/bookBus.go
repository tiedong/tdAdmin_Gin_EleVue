package business

import (
	"errors"
	"vueBasic/app/admin/filter"
	"vueBasic/app/common/model"
	"vueBasic/extend/conv"
)

var BookB = new(bookBus)

type bookBus struct {
}

func (*bookBus) GetPageList(req *filter.BookReq) (any, int64, error) {
	bookList, cnt, err := model.BookM.GetList(req)
	return bookList, cnt, err
}

func (*bookBus) Add(req *filter.BookAddReq) (uint, error) {
	if conv.IsEmpty(req.Title) {
		return 0, errors.New("标题不能为空！")
	}
	req.AddTime = conv.NowUnix()
	req.UpdateTime = conv.NowUnix()
	res, err := model.BookM.Add(req)
	if err != nil {
		return 0, err
	}
	return res, nil
}

func (*bookBus) GetUpdateInfo(req *filter.BookGetUpdateInfoReq) (any, error) {
	info, err := model.BookM.GetBookById(conv.Int(req.Id))
	if err != nil {
		return nil, err
	}
	return info, nil
}

func (*bookBus) Update(req *filter.BookUpdateReq) (uint, error) {
	req.UpdateTime = conv.NowUnix()
	res, err := model.BookM.Update(req)
	if err != nil {
		return 0, err
	}
	return res, nil
}

func (*bookBus) UpdateExt(req map[string]interface{}) error {
	err := model.BookM.UpdateExt(req)
	if err != nil {
		return err
	}
	return nil
}

func (*bookBus) Delete(req *filter.BookDeleteReq) (uint, error) {
	ids := conv.Slice(req.Id)
	res, err := model.BookM.Delete(ids)
	if err != nil {
		return 0, err
	}
	return res, nil
}
