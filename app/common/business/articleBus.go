package business

import (
	"errors"
	"vueBasic/app/admin/filter"
	"vueBasic/app/common/model"
	"vueBasic/extend/conv"
)

var ArticleB = new(articleBus)

type articleBus struct {
}

func (*articleBus) GetPageList(req *filter.ArticleReq) (any, int64, error) {
	articleList, cnt, err := model.ArticleM.GetList(req)
	return articleList, cnt, err
}

func (*articleBus) Add(req *filter.ArticleAddReq) (uint, error) {
	if conv.IsEmpty(req.Title) {
		return 0, errors.New("标题不能为空！")
	}
	if conv.IsEmpty(req.ArticleCateId) {
		return 0, errors.New("请选择分类！")
	}
	req.AddTime = conv.NowUnix()
	req.UpdateTime = conv.NowUnix()
	res, err := model.ArticleM.Add(req)
	if err != nil {
		return 0, err
	}
	return res, nil
}

func (*articleBus) GetUpdateInfo(req *filter.ArticleCateGetUpdateInfoReq) (any, error) {
	info, err := model.ArticleM.GetArticleById(conv.Int(req.Id))
	if err != nil {
		return nil, err
	}
	return info, nil
}

func (*articleBus) Update(req *filter.ArticleUpdateReq) (uint, error) {
	req.UpdateTime = conv.NowUnix()
	res, err := model.ArticleM.Update(req)
	if err != nil {
		return 0, err
	}
	return res, nil
}

func (*articleBus) UpdateExt(req map[string]interface{}) error {
	err := model.ArticleM.UpdateExt(req)
	if err != nil {
		return err
	}
	return nil
}

func (*articleBus) Delete(req *filter.ArticleDeleteReq) (uint, error) {
	ids := conv.Slice(req.Id)
	res, err := model.ArticleM.Delete(ids)
	if err != nil {
		return 0, err
	}
	return res, nil
}
