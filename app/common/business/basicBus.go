package business

import (
	"vueBasic/app/admin/filter"
	"vueBasic/app/common/model"
	"vueBasic/extend/conv"
	"vueBasic/extend/utils"
)

var BasicB = new(basicBus)

type basicBus struct {
}

// 获取菜单列表
func (*basicBus) GetMenuList(role_id int) any {
	access := make([]string, 0)
	if role_id != 9 { //不是超管的检查
		access = model.RoleAccessM.GetValueByRoleId(role_id, 0)
		if conv.IsEmpty(access) {
			return nil
		}
	}
	req := filter.AdminMenuReq{
		Type:      1,
		Status:    1,
		AccessIds: access,
	}
	menuList := BasicB.getMenuList(&req)
	return menuList
}

// 获取角色菜单列表
func (*basicBus) GetRoleMenus() any {
	req := filter.AdminMenuReq{}
	menuList := BasicB.getMenuList(&req)
	return menuList
}

func (*basicBus) getMenuList(req *filter.AdminMenuReq) any {
	accessList := model.AccessM.GetList(req)
	List := make([]*filter.AccessNode, 0)
	for _, v := range accessList {
		List = append(List, &filter.AccessNode{
			Id:         v.Id,
			ModuleId:   v.ModuleId,
			Url:        v.Url,
			Icon:       v.Icon,
			ModuleName: v.ModuleName,
			Sort:       v.Sort,
			Children:   nil,
		})
	}
	menuList := utils.GetTreeRecursive(List, 0, 0)
	return menuList
}

func (*basicBus) DeleteFile(req *filter.AdminFileDeleteReq) error {
	filepath := conv.Slice(req.Filepath)
	err := model.FileM.Delete(filepath)
	return err
}
