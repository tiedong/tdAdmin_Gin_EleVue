package business

import (
	"errors"
	"vueBasic/app/admin/filter"
	"vueBasic/app/common/model"
	"vueBasic/extend/conv"
)

var LogB = new(LogBus)

type LogBus struct {
}

func (*LogBus) GetPageList(req *filter.AdminLogListReq) (any, int64, error) {

	list, count, err := model.AdminLogM.GetList(req)
	return list, count, err
}

func (*LogBus) GetUpdateInfo(req *filter.AdminLogInfoReq) (any, error) {
	info, err := model.AdminLogM.GetLogInfoById(req)
	if err != nil {
		return nil, errors.New("用户不存在")
	}
	return info, nil
}

func (*LogBus) Delete(req *filter.AdminLogDeleteReq) (uint, error) {
	ids := conv.Slice(req.Id)
	res, err := model.AdminLogM.Delete(ids)
	if err != nil {
		return 0, errors.New("删除失败")
	}
	return res, nil
}
