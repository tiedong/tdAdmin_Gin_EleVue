package business

import (
	"errors"
	"vueBasic/app/common/model"
	"vueBasic/extend/conv"
)

var BaseConfigB = new(baseconfigBus)

type baseconfigBus struct {
}

func (*baseconfigBus) GetInfo() (map[string]interface{}, error) {
	data := make(map[string]interface{}, 0)
	info, err := model.ConfigM.GetInfo()
	if err != nil {
		return nil, errors.New("配置信息查询错误")
	}
	for _, v := range info {
		data[v.Name] = v.Data
	}
	return data, nil
}

func (*baseconfigBus) Update(req map[string]interface{}) error {
	//先获取字段数据
	column := model.ConfigM.GetColumn("name")
	for key, val := range req {
		if conv.IsValueInList(key, column) { //更新
			if err := model.ConfigM.Update(key, val); err != nil {
				return err
			}
		} else { //添加
			if err := model.ConfigM.Add(key, val); err != nil {
				return err
			}
		}
	}
	return nil
}
