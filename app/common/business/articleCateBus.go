package business

import (
	"errors"
	"vueBasic/app/admin/filter"
	"vueBasic/app/common/model"
	"vueBasic/extend/conv"
)

var ArticleCateB = new(articleCateBus)

type articleCateBus struct {
}

func (*articleCateBus) GetPageList(req *filter.ArticleCateReq) (any, int64, error) {
	cateList, cnt, err := model.ArticleCateM.GetList(req)
	list := make([]*filter.ArticleCateListRes, 0)
	for _, v := range cateList {
		list = append(list, &filter.ArticleCateListRes{
			Id:         v.Id,
			Title:      v.Title,
			Sort:       v.Sort,
			Keywords:   v.Keywords,
			Desc:       v.Desc,
			AddTime:    conv.DateTime(v.AddTime),
			UpdateTime: conv.DateTime(v.UpdateTime),
		})
	}
	return list, cnt, err
}

func (*articleCateBus) Add(req *filter.ArticleCateAddReq) (uint, error) {
	req.AddTime = conv.NowUnix()
	req.UpdateTime = conv.NowUnix()
	res, err := model.ArticleCateM.Add(req)
	if err != nil {
		return 0, err
	}
	return res, nil
}

func (*articleCateBus) GetUpdateInfo(req *filter.ArticleCateGetUpdateInfoReq) (any, error) {
	info, err := model.ArticleCateM.GetArticleCateById(conv.Int(req.Id))
	if err != nil {
		return nil, err
	}
	return info, nil
}

func (*articleCateBus) Update(req *filter.ArticleCateUpdateReq) (uint, error) {
	req.UpdateTime = conv.NowUnix()
	res, err := model.ArticleCateM.Update(req)
	if err != nil {
		return 0, err
	}
	return res, nil
}

func (*articleCateBus) UpdateExt(req map[string]interface{}) error {
	err := model.ArticleCateM.UpdateExt(req)
	if err != nil {
		return err
	}
	return nil
}

func (*articleCateBus) Delete(req *filter.ArticleCateDeleteReq) (uint, error) {
	ids := conv.Slice(req.Id)
	if len(ids) > 1 {
		return 0, errors.New("禁止批量删除")
	}
	res, err := model.ArticleCateM.Delete(ids)
	if err != nil {
		return 0, err
	}
	return res, nil
}

func (*articleCateBus) GetFieldList() (map[string]interface{}, error) {
	field := "id,pid,title"
	AccessList, err := model.ArticleCateM.GetFieldList(field)
	if err != nil {
		return nil, err
	}
	list := make([]*filter.ArticleCatePid, 0)
	for _, v := range AccessList {
		list = append(list, &filter.ArticleCatePid{
			Id:       conv.Int(v.Id),
			Title:    v.Title,
			Pid:      v.Pid,
			Children: nil,
		})
	}
	data := make(map[string]interface{}, 0)
	data["pids"] = list
	return data, nil
}
