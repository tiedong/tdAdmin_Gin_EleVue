package model

import "vueBasic/global"

var ConfigM = new(configModel)

/**
 * @description(基本配置模型)
 * @buildcode(true)
 */
type configModel struct {
	Name string `json:"name"`
	Data string `json:"data"`
}

func (*configModel) TableName() string {
	return global.CONF.Database.Db.Prefix + "config"
}

func (*configModel) GetInfo() ([]*configModel, error) {
	var (
		entity []*configModel
	)
	if err := global.DB.Find(&entity).Error; err != nil {
		return nil, err
	}
	return entity, nil
}

// 获取字段
func (*configModel) GetColumn(key string) []string {
	column := make([]string, 0)
	var entity configModel
	global.DB.Model(&entity).Pluck(key, &column)
	return column
}

// 添加
func (*configModel) Add(key string, name interface{}) error {
	entity := configModel{}
	if err := global.DB.Model(&entity).Create(map[string]interface{}{"name": key, "data": name}).Error; err != nil {
		return err
	}
	return nil
}

// 更行
func (*configModel) Update(key string, name interface{}) error {
	entity := configModel{}
	if err := global.DB.Model(&entity).Where("name=?", key).Update("data", name).Error; err != nil {
		return err
	}
	return nil
}
