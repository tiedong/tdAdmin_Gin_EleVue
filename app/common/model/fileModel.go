package model

import (
	"vueBasic/app/admin/filter"
	"vueBasic/global"
)

var FileM = new(fileModel)

type fileModel struct {
	Id         uint   `gorm:"primaryKey" json:"id"` //id
	Filepath   string `json:"filepath"`             //图片路径
	Hash       string `json:"hash"`                 //文件hash值
	CreateTime int64  `json:"create_time"`          //创建时间
	Disk       string `json:"disk"`                 //存储方式
	Type       int8   `json:"type"`                 //文件类型
}

func (*fileModel) TableName() string {
	return global.CONF.Database.Db.Prefix + "file"
}

// 添加
func (*fileModel) Add(req filter.AdminFileAddReq) error {
	entity := fileModel{}
	entity.Filepath = req.Filepath
	entity.Hash = req.Hash
	entity.Disk = req.Disk
	entity.CreateTime = req.CreateTime
	if err := global.DB.Create(&entity).Error; err != nil {
		return err
	}
	return nil
}

// 删除
func (*fileModel) Delete(filepath []string) error {
	query := global.DB.Where("filepath IN ?", filepath)
	if err := query.Delete(&fileModel{}).Error; err != nil {
		return err
	}
	return nil
}
