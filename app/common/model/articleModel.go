package model

import (
	"gorm.io/gorm"
	"vueBasic/app/admin/filter"
	"vueBasic/extend/conv"
	"vueBasic/global"
)

var ArticleM = new(articleModel)

type articleModel struct {
	Id            uint              `form:"id" json:"id"`
	Title         string            `form:"title" json:"title"` //标题
	Keywords      string            `form:"keywords" json:"keywords"`
	Desc          string            `form:"desc" json:"desc"`
	Status        int               `form:"status" json:"status"`
	Thumb         string            `form:"thumb" json:"thumb"`
	Original      int               `form:"original" json:"original"`
	Origin        string            `form:"origin" json:"origin"`
	OriginUrl     string            `form:"origin_url" json:"origin_url"`
	Content       string            `form:"content" json:"content"`
	Read          int               `form:"read" json:"read"`
	Type          int               `form:"type" json:"type"`
	IsHome        int               `form:"is_home" json:"is_home"`
	Sort          int               `form:"sort" json:"sort"`
	ArticleCateId int               `form:"article_cate_id" json:"article_cate_id"`
	AddTime       int64             `form:"add_time" json:"add_time"`
	UpdateTime    int64             `form:"update_time" json:"update_time"`
	DeleteTime    int64             `form:"delete_time" json:"delete_time"`
	ArticleCate   *articleCateModel `json:"article_cate" grom:"foreignKey:ArticleCateId,references:Id"` //关联模型
}

func (*articleModel) TableName() string {
	return global.CONF.Database.Db.Prefix + "article"
}

func (*articleModel) GetList(req *filter.ArticleReq) ([]articleModel, int64, error) {
	var (
		articleList []articleModel
		count       int64
	)
	query := global.DB.Model(&articleModel{})
	if !conv.IsEmpty(req.Title) {
		query = query.Where("title LIKE ?", req.Title)
	}
	if !conv.IsEmpty(req.ArticleCateId) {
		query = query.Where("article_cate_id=?", req.ArticleCateId)
	}
	if !conv.IsEmpty(req.Status) {
		query = query.Where("status=?", req.Status)
	}
	if !conv.IsEmpty(req.AddTime) {
		query = query.Where("add_time BETWEEN ? AND ?", conv.UnixTime(req.AddTime[0]), conv.UnixTime(req.AddTime[1]))
	}
	query = query.Preload("ArticleCate", func(db *gorm.DB) *gorm.DB {
		return db.Select("id,title")
	})
	if err := query.Limit(req.Limit).Offset((req.Page - 1) * req.Limit).Order("sort ASC").Find(&articleList).Error; err != nil {
		return nil, 0, err
	}
	//获取总条数
	query.Count(&count)
	return articleList, count, nil
}

func (*articleModel) Add(req *filter.ArticleAddReq) (uint, error) {
	entity := articleModel{}
	entity.Title = req.Title
	entity.Sort = req.Sort
	entity.Keywords = req.Keywords
	entity.Desc = req.Desc
	entity.Status = req.Status
	entity.Thumb = req.Thumb
	entity.Type = req.Type
	entity.Original = req.Original
	entity.Origin = req.Origin
	entity.OriginUrl = req.OriginUrl
	entity.ArticleCateId = req.ArticleCateId
	entity.IsHome = req.IsHome
	entity.Read = 0
	entity.Content = req.Content
	entity.AddTime = req.AddTime
	entity.UpdateTime = req.UpdateTime
	entity.DeleteTime = 0
	if err := global.DB.Create(&entity).Error; err != nil {
		return 0, err
	}
	return entity.Id, nil
}

func (*articleModel) GetArticleById(id int) (*articleModel, error) {
	entity := articleModel{}
	if err := global.DB.Where("id=?", id).Limit(1).First(&entity).Error; err != nil {
		return nil, err
	}
	return &entity, nil
}

func (*articleModel) Update(req *filter.ArticleUpdateReq) (uint, error) {
	entity := articleModel{Id: req.Id}
	global.DB.First(&entity)
	entity.Title = req.Title
	entity.Sort = req.Sort
	entity.Keywords = req.Keywords
	entity.Desc = req.Desc
	entity.Status = req.Status
	entity.Thumb = req.Thumb
	entity.Type = req.Type
	entity.Original = req.Original
	entity.Origin = req.Origin
	entity.OriginUrl = req.OriginUrl
	entity.ArticleCateId = req.ArticleCateId
	entity.IsHome = req.IsHome
	entity.Content = req.Content
	entity.UpdateTime = req.UpdateTime
	if err := global.DB.Save(&entity).Error; err != nil {
		return 0, err
	}
	return entity.Id, nil
}

func (*articleModel) UpdateExt(req map[string]interface{}) error {
	query := global.DB.Model(&articleModel{})
	if err := query.Where("id=?", req["id"]).Updates(req).Error; err != nil {
		return err
	}
	return nil
}

func (*articleModel) Delete(ids []string) (uint, error) {
	query := global.DB.Where("id IN ?", ids)
	err := query.Delete(&articleModel{}).Error
	if err != nil {
		return 0, err
	}
	return 1, nil
}
