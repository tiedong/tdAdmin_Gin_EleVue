package model

import (
	"fmt"
	"vueBasic/app/admin/filter"
	"vueBasic/extend/conv"
	"vueBasic/global"
)

var AccessM = new(accessModel)

type accessModel struct {
	Id          uint   `form:"id" json:"id"`
	ModuleName  string `form:"module_name" json:"module_name"` //模块名称
	ActionName  string `form:"action_name" json:"action_name"` //操作名称
	Type        int    `form:"type" json:"type"`               //节点类型 :  1、表示模块    2、表示菜单     3、操作
	Url         string `form:"url" json:"url"`                 //路由跳转地址
	Icon        string `form:"icon" json:"icon"`               //图标
	ModuleId    int    `form:"module_id" json:"module_id"`     //此module_id和当前模型的id关联       module_id= 0 表示模块
	Sort        int    `form:"sort" json:"sort"`
	Description string `form:"description" json:"description"`
	Status      int    `form:"status" json:"status"`
	AddTime     int64  `form:"add_time" json:"add_time"`
	UpdateTime  int64  `form:"update_time" json:"update_time"`
}

func (*accessModel) TableName() string {
	return global.CONF.Database.Db.Prefix + "access"
}

func (*accessModel) GetList(req *filter.AdminMenuReq) []accessModel {
	var (
		accessList []accessModel
	)
	query := global.DB.Model(&accessModel{})
	if !conv.IsEmpty(req.Type) {
		query.Where("type=?", req.Type)
	}
	if !conv.IsEmpty(req.Status) {
		query.Where("status=?", req.Status)
	}
	if !conv.IsEmpty(req.AccessIds) {
		query.Where("id in (?)", req.AccessIds)
	}
	query.Order("sort ASC").Find(&accessList)
	return accessList

}

// 根据分页获取数据
func (*accessModel) GetPageList(req *filter.AdminAccessPageReq) ([]*accessModel, int64, error) {
	var (
		list  []*accessModel
		count int64
	)
	query := global.DB.Model(&accessModel{})
	if !conv.IsEmpty(req.Id) {
		query = query.Where("Id=?", req.Id)
	}
	order := "sort asc"
	if !conv.IsEmpty(req.Sort) && !conv.IsEmpty(req.Order) {
		order = fmt.Sprintf("%s %s", req.Sort, req.Order)
	}
	if err := query.Limit(req.Limit).Offset((req.Page - 1) * req.Limit).Order(order).Find(&list).Error; err != nil {
		return nil, 0, err
	}
	query.Count(&count)
	return list, count, nil
}

func (*accessModel) GetFieldList(field string) ([]*accessModel, error) {
	var list []*accessModel
	query := global.DB.Model(&accessModel{})
	if err := query.Select(field).Where("status=1").Scan(&list).Error; err != nil {
		return nil, err
	}
	return list, nil
}

// 添加
func (*accessModel) Add(req *filter.AdminAccessAddReq) (uint, error) {
	entity := accessModel{}
	entity.ModuleName = req.ModuleName
	entity.ActionName = ""
	entity.Status = req.Status
	entity.ModuleId = req.ModuleId
	entity.Type = req.Type
	entity.Sort = req.Sort
	entity.Icon = req.Icon
	entity.Url = req.Url
	entity.Description = req.Description
	entity.AddTime = conv.NowUnix()
	entity.UpdateTime = conv.NowUnix()
	if err := global.DB.Create(&entity).Error; err != nil {
		return 0, err
	}
	return entity.Id, nil
}

func (*accessModel) GetAccessById(req *filter.AdminAccessGetUpdateInfoReq) (*accessModel, error) {
	entity := accessModel{}
	if err := global.DB.Where("id=?", req.Id).Limit(1).First(&entity).Error; err != nil {
		return nil, err
	}
	return &entity, nil
}

// 更新
func (*accessModel) Update(req *filter.AdminAccessUpdateReq) (uint, error) {
	entity := accessModel{Id: req.Id}
	global.DB.First(&entity)
	entity.ModuleName = req.ModuleName
	entity.ActionName = req.ActionName
	entity.Status = req.Status
	entity.ModuleId = req.ModuleId
	entity.Type = req.Type
	entity.Sort = req.Sort
	entity.Icon = req.Icon
	entity.Url = req.Url
	entity.Description = req.Description
	entity.UpdateTime = conv.NowUnix()
	if err := global.DB.Save(&entity).Error; err != nil {
		return 0, err
	}
	return entity.Id, nil
}

func (*accessModel) UpdateExt(req map[string]interface{}) error {
	query := global.DB.Model(&accessModel{})
	if err := query.Where("id=?", req["id"]).Updates(req).Error; err != nil {
		return err
	}
	return nil
}

// 删除
func (*accessModel) Delete(ids []string) (uint, error) {
	query := global.DB.Where("id IN ?", ids)
	err := query.Delete(&accessModel{}).Error
	if err != nil {
		return 0, err
	}
	return 1, nil
}

func (*accessModel) GetColumnByIds(ids []string, column string) []string {
	value := make([]string, 0)
	query := global.DB.Model(&accessModel{}).Where("id IN (?)", ids).Where("module_id != ?", 0)
	query.Pluck(column, &value)
	return value
}
