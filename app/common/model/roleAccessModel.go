package model

import (
	"vueBasic/app/admin/filter"
	"vueBasic/global"
)

var RoleAccessM = new(roleAccessModel)

/**
 * @description(基本配置模型)
 * @buildcode(true)
 */
type roleAccessModel struct {
	RoleId   int `json:"role_id"`
	AccessId int `json:"access_id"`
}

func (*roleAccessModel) TableName() string {
	return global.CONF.Database.Db.Prefix + "role_access"
}

// 获取字段
func (*roleAccessModel) GetValueByRoleId(role_id int, is_del_type0 int) []string {
	value := make([]string, 0)
	query := global.DB.Model(&roleAccessModel{}).Where("role_id = ?", role_id)
	query.Pluck("access_id", &value)
	if is_del_type0 != 0 { //is_del_type0不等于0剔除module_id=0
		value = AccessM.GetColumnByIds(value, "id")
	}
	return value
}

// 删除
func (*roleAccessModel) Delete(id int) error {
	query := global.DB.Where("role_id = ?", id)
	err := query.Delete(&roleAccessModel{}).Error
	if err != nil {
		return err
	}
	return nil
}

// 添加
func (*roleAccessModel) Add(data []*filter.AdminRoleAccessReq) error {

	entity := roleAccessModel{}
	if err := global.DB.Model(&entity).Create(&data).Error; err != nil {
		return err
	}
	return nil
}
