package global

import (
	"github.com/go-redis/redis/v8"
	"go.uber.org/zap"
	"gorm.io/gorm"
	"vueBasic/config"
)

var (
	DB         *gorm.DB
	GVA_DBLIST map[string]*gorm.DB
	CONF       config.Config      //默认配置实例
	LOG        *zap.SugaredLogger //默认日志实例
	ACCESSLOG  *zap.SugaredLogger //访问日志实例
	REDISDb    *redis.Client
)
