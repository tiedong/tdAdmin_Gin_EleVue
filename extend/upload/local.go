package upload

import (
	"github.com/gin-gonic/gin"
	"mime/multipart"
	"vueBasic/extend/utils"
	"vueBasic/global"
)

var Local = new(local)

type local struct{}

// 本地上传文件
func (local) Upload(file *multipart.FileHeader, filename string, c *gin.Context) string {
	//判断是否有创建文件夹
	if utils.CreatePath(utils.GetSavePath(c)) {
		if err := c.SaveUploadedFile(file, global.CONF.AccessPath+filename); err != nil {
			panic(err)
		}
	}
	return filename
}
