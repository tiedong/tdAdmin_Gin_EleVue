package cache

import (
	"github.com/muesli/cache2go"
	"strings"
	"time"
	"vueBasic/extend/conv"
)

//或者可以使用gocache（github.com/patrickmn/go-cache）插件库

var Cache2go = new(cachego)

var cache2goHandle = cache2go.Cache("myCache")

type cachego struct {
}

func (c cachego) Set(key string, value string, expireTime time.Duration) error {
	cache2goHandle.Add(key, expireTime, &value)
	return nil
}

func (c cachego) Get(key string) string {
	res, err := cache2goHandle.Value(key)
	if err == nil {
		return conv.String(res.Data())
	}
	return ""
}

// 设置缓存
func (c cachego) Setx(key string, value interface{}, expireTime time.Duration) error {
	cache2goHandle.Add(key, expireTime, &value)
	return nil
}

// 获取缓存
func (c cachego) Getx(key string) interface{} {
	value, err := cache2goHandle.Value(key)
	if err == nil {
		return value
	}
	return nil
}

func (c cachego) Delete(key string) int64 {
	cache2goHandle.Delete(key)
	return 1
}

// 批量模糊删除key值，例如：key=abac  那么值可以以ab开头之类
func (c cachego) DeleteByLike(key string) int64 {
	keysArr := make([]string, 0)
	cache2goHandle.Foreach(func(skey interface{}, item *cache2go.CacheItem) {
		if strings.HasPrefix(conv.String(skey), key) {
			keysArr = append(keysArr, key)
		}
	})
	for _, k := range keysArr {
		cache2goHandle.Delete(k)
	}
	return 1
}

// 清空缓存
func (c cachego) Flush() {
	cache2goHandle.Flush()
}

func (c cachego) Remember(key string, ttl time.Duration, f func() (string, error)) (string, error) {
	var err error
	res := c.Get(key)
	if res != "" {
		return res, nil
	} else {
		res, err = f()
		if err == nil {
			c.Set(key, res, ttl)
		}
	}
	return res, err
}
