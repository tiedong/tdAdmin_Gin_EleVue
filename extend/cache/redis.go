package cache

import (
	"context"
	"go.uber.org/zap"
	"time"
	"vueBasic/global"
)

var Redis = new(redis)

type redis struct {
}

// 设置缓存
func (c redis) Set(key string, value string, expireTime time.Duration) error {
	if err := global.REDISDb.Set(context.Background(), key, value, expireTime).Err(); err != nil {
		global.LOG.Error("[缓存异常：]",
			zap.String("key", key),
			zap.Any("value", value),
		)
	}
	return nil
}

// 获取缓存
func (c redis) Get(key string) string {
	value, _ := global.REDISDb.Get(context.Background(), key).Result()
	return value
}

// hash表获取全部
func (c redis) HGetAll(key string) map[string]string {
	res, _ := global.REDISDb.HGetAll(context.Background(), key).Result()
	return res
}

// 获取hash表单个值
func (c redis) HGet(key string, field string) string {
	res, _ := global.REDISDb.HGet(context.Background(), key, field).Result()
	return res
}

// hash表操作
func (c redis) HSet(key string, field string, value string) {
	global.REDISDb.HSet(context.Background(), key, field, value).Result()
}

// hash表操作
func (c redis) HDelete(key string, field string) {
	global.REDISDb.HDel(context.Background(), key, field).Result()
}

func (c redis) Delete(key string) int64 {
	res, _ := global.REDISDb.Del(context.Background(), key).Result()
	return res
	return 0
}

func (c redis) DeleteByLike(key string) int64 {
	keys, _ := global.REDISDb.Keys(context.Background(), key).Result()
	if len(keys) > 0 {
		res, _ := global.REDISDb.Del(context.Background(), keys...).Result()
		return res
	}
	return 0
}

func (c redis) Remember(key string, ttl time.Duration, f func() (string, error)) (string, error) {
	var err error
	res := c.Get(key)
	if res != "" {
		return res, nil
	} else {
		res, err = f()
		if err == nil {
			c.Set(key, res, ttl)
		}
	}
	return res, err
}

// redis加锁
func (c redis) AcquireLock(key string, val string, expiration time.Duration) (bool, error) {
	result, err := global.REDISDb.SetNX(context.Background(), key, val, expiration).Result()
	if err != nil {
		return false, err
	}
	return result, nil
}
