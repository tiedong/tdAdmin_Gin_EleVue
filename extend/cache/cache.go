package cache

import (
	"time"
	"vueBasic/global"
)

// 定义一个缓存接口
type cacheInterface interface {
	Set(key string, val string, expireTime time.Duration) error
	Get(key string) string
	Delete(key string) int64
	DeleteByLike(key string) int64
	Remember(key string, ttl time.Duration, f func() (string, error)) (string, error)
}

// 返回接口实例
func New(driver ...string) cacheInterface {
	if global.CONF.Cache.Driver == "cache2go" || (len(driver) > 0 && driver[0] == "cache2go") {
		return Cache2go
	} else {
		return Redis
	}
}
