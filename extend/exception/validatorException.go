package e

// 自定义验证器错误
type ValidateError string

func (e ValidateError) Error() string {
	return string(e)
}
