package event

import (
	"github.com/gookit/event"
	"vueBasic/extend/listen"
)

// 注册事件监听器
func RegisterEvent() {
	event.On("adminLoginEvent", event.ListenerFunc(listen.AdminLoginEventListen), event.Normal)     //后台登陆日志
	event.On("exceptionLogEvent", event.ListenerFunc(listen.ExceptionLogEventListen), event.Normal) //捕获异常
	event.On("afterUploadEvent", event.ListenerFunc(listen.CreateSaveFilepath), event.Normal)       //上传图片
}
