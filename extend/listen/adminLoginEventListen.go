package listen

import (
	"github.com/gin-gonic/gin"
	"github.com/gookit/event"
	"strings"
	"vueBasic/app/admin/filter"
	"vueBasic/app/common/model"
	"vueBasic/extend/conv"
	"vueBasic/extend/utils"
	"vueBasic/global"
)

// 监听登陆，写入登录日志
func AdminLoginEventListen(e event.Event) error {
	handle := e.Data()["handle"].(*gin.Context)
	var logData filter.AdminLogAddReq
	logData.ApplicationName = strings.Split(strings.TrimLeft(handle.Request.RequestURI, "/"), "/")[0]
	logData.Username = e.Data()["user"].(string)
	logData.Ip = utils.GetClientIP(handle)
	logData.Url = "http://" + handle.Request.Host + handle.Request.RequestURI
	logData.Useragent = handle.Request.UserAgent()
	logData.Type = 1
	logData.CreateTime = conv.NowUnix()
	if err := model.AdminLogM.Add(logData); err != nil {
		global.LOG.Error(err.Error())
	}
	return nil
}
