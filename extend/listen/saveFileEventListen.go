package listen

import (
	"github.com/gookit/event"
	"vueBasic/app/admin/filter"
	"vueBasic/app/common/model"
	"vueBasic/extend/conv"
	"vueBasic/global"
)

// 图片保存
func CreateSaveFilepath(e event.Event) error {
	var fileData filter.AdminFileAddReq
	fileData.Filepath = e.Data()["filepath"].(string)
	fileData.Hash = e.Data()["hash"].(string)
	fileData.Disk = global.CONF.Disks
	fileData.CreateTime = conv.NowUnix()
	if err := model.FileM.Add(fileData); err != nil {
		global.LOG.Error(err.Error())
	}
	return nil
}
