package captcha

import (
	"github.com/mojocn/base64Captcha"
	"vueBasic/global"
)

type configJsonBody struct {
	Id          string
	CaptchaType string
	VerifyValue string
	DriverDigit *base64Captcha.DriverDigit
	//可以添加中文类型的验证码
}

// 设置自带的 store
var store = base64Captcha.DefaultMemStore

// 获取验证码
func MakeCaptcha() map[string]interface{} {
	param := configJsonBody{
		Id:          "",
		CaptchaType: "digit",
		VerifyValue: "",
		DriverDigit: &base64Captcha.DriverDigit{
			Height:   global.CONF.Captcha.ImgHeight,
			Width:    global.CONF.Captcha.ImgWidth,
			Length:   global.CONF.Captcha.KeyLong,
			MaxSkew:  0.1,
			DotCount: 80,
		},
	}
	var driver base64Captcha.Driver
	switch param.CaptchaType {
	//case "string":
	//	driver = param.DriverString.ConvertFonts()
	default:
		driver = param.DriverDigit
	}
	if global.CONF.Cache.Driver == "redis" {
		store = RedisStore{}
	}
	//创建 Captcha
	captcha := base64Captcha.NewCaptcha(driver, store)
	//Generate 生成随机 id、base64 图像字符串
	id, lb64s, _, _ := captcha.Generate()
	data := make(map[string]interface{})
	data["key"] = id
	data["base64"] = lb64s
	return data
}

// 验证
func Verify(id string, VerifyValue string) (res bool) {
	param := configJsonBody{
		Id:          id,
		CaptchaType: "digit",
		VerifyValue: VerifyValue,
		DriverDigit: &base64Captcha.DriverDigit{
			Height:   global.CONF.Captcha.ImgHeight,
			Width:    global.CONF.Captcha.ImgWidth,
			Length:   global.CONF.Captcha.KeyLong,
			MaxSkew:  0.7,
			DotCount: 80,
		},
	}
	if global.CONF.Cache.Driver == "redis" {
		store = RedisStore{}
	} else {
		store = base64Captcha.DefaultMemStore
	}
	return store.Verify(param.Id, param.VerifyValue, true)
}
