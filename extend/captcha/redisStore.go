package captcha

import (
	"context"
	"fmt"
	"time"
	"vueBasic/global"
)

var ctx1 = context.Background()

const CAPTCHA = "captcha:"

type RedisStore struct{}

// 实现设置captcha的方法
func (r RedisStore) Set(id string, value string) error {
	key := CAPTCHA + id
	err := global.REDISDb.Set(ctx1, key, value, time.Minute*2).Err()

	return err
}

// 实现获取captcha的方法
func (r RedisStore) Get(id string, clear bool) string {
	key := CAPTCHA + id
	val, err := global.REDISDb.Get(ctx1, key).Result()
	if err != nil {
		fmt.Println(err)
		return ""
	}
	if clear {
		err := global.REDISDb.Del(ctx1, key).Err()
		if err != nil {
			fmt.Println(err)
			return ""
		}
	}
	return val
}

func (r RedisStore) Verify(id, answer string, clear bool) bool {
	v := RedisStore{}.Get(id, clear)
	return v == answer
}
