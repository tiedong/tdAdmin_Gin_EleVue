Vue.component('Detail', {
	template: `
		<el-dialog title="查看详情" width="600px" class="icon-dialog" :visible.sync="show" @open="open" :before-close="closeForm" append-to-body>
			<table cellpadding="0" cellspacing="0" class="table table-bordered" align="center" width="100%" style="word-break:break-all; margin-bottom:30px;  font-size:13px;">
				<tbody>
					<tr>
						<td class="title" width="100">用户姓名：</td>
						<td>
							{{form.nickname}}
						</td>
					</tr>
					<tr>
						<td class="title" width="100">用户名：</td>
						<td>
							{{form.username}}
						</td>
					</tr>
					<tr>
						<td class="title" width="100">手机号码：</td>
						<td>
							{{form.mobile}}
						</td>
					</tr>
					<tr>
						<td class="title" width="100">邮箱：</td>
						<td>
							{{form.email}}
						</td>
					</tr>
					<tr>
						<td class="title" width="100">qq：</td>
						<td>
							{{form.qq}}
						</td>
					</tr>
					<tr>
						<td class="title" width="100">所属分组：</td>
						<td>
							<span v-if="form.name">{{form.name}}</span>
						</td>
					</tr>
					<tr>
						<td class="title" width="100">状态：</td>
						<td>
							<span v-if="form.status == '1'">正常</span>
							<span v-if="form.status == '0'">禁用</span>
						</td>
					</tr>
					<tr>
						<td class="title" width="100">创建时间：</td>
						<td>
							{{parseTime(form.add_time,'{y}-{m}-{d}')}}
						</td>
					</tr>
				</tbody>
			</table>
		</el-dialog>
	`
	,
	props: {
		show: {
			type: Boolean,
			default: true
		},
		size: {
			type: String,
			default: 'mini'
		},
		info: {
			type: Object,
		},
	},
	data() {
		return {
			form:{
			},
		}
	},
	methods: {
		open(){
			axios.post(base_url+'/admin/adminuser/getUpdateInfo',this.info).then(res => {
				console.log(res.data.data.role.title)
				this.form = res.data.data
				if (res.data.data.role.title != null){
					this.form.name = res.data.data.role.title
				}
			})
		},
		closeForm(){
			this.$emit('update:show', false)
		}
	}
})
