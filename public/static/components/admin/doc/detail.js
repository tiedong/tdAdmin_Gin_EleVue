Vue.component('Detail', {
	template: `
		<div v-if="show">
			<el-form :size="size" ref="form" :model="form" :rules="rules"">
			<el-tabs v-model="activeName">
				<el-row >
					<el-col :span="24">
						<el-form-item label="" prop="content" v-if="show">
							<tinymce  :content.sync="form.content"></tinymce>
						</el-form-item>
					</el-col>
				</el-row>
			</el-tabs>
				<el-form-item>
					<el-button :size="size" type="primary" @click="submit">保存</el-button>
				</el-form-item>
			</el-form>
		</div>
	`
	,
	components:{
	},
	props: {
		show: {
			type: Boolean,
			default: false
		},
		size: {
			type: String,
			default: 'small'
		},
		info: {
			type: Object,
		},
	},
	data(){
		return {
			form: {
				id:0,
				content:'<p>请点击左边的菜单获取内容</p>',
			},
			loading:false,
			activeName:'',
			rules: {
			}
		}
	},
	watch:{
		show(val){
			if(val){
				this.open()
			}
		}
	},
	methods: {
		open(){
			this.form.id = this.info.id
			this.form.content = this.info.content
		},
		submit(){
			this.$refs['form'].validate(valid => {
				if(valid) {
					this.loading = true
					axios.post(base_url + '/admin/doc/updateExt',this.form).then(res => {
						if(res.data.status == 200){
							this.$message({message: res.data.msg, type: 'success'})
						}else{
							this.loading = false
							this.$message.error(res.data.msg)
						}
					}).catch(()=>{
						this.loading = false
					})
				}
			})
		},
		setDefaultVal(key){
			if(this.form[key] == null || this.form[key] == ''){
				this.form[key] = []
			}
		}
	}
})
