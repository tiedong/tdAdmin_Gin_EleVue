Vue.component('Auth', {
	template: `
		<el-dialog title="授权" width="600px" class="icon-dialog" :visible.sync="show" @open="open" :before-close="closeForm" append-to-body>
			<el-form :size="size" ref="form" :model="form"  :label-width=" ismobile()?'90px':'16%'">
				<el-row >
					<el-col :span="24">
						<el-form-item label="角色名称" prop="title">
							<el-input  v-model="form.title" autoComplete="off" disabled=""></el-input>
						</el-form-item>
					</el-col>
				</el-row>
				<el-row>
					<el-col :span="24">
						<el-form-item label="菜单权限" prop="role_type">
<!--							<el-tree class="tree-border" @check="checkStrictly" :data="options" show-checkbox default-expand-all ref="menu" node-key="id"  :default-checked-keys="form.access" :check-strictly="strictly" empty-text="加载中，请稍后" :props="defaultProps"></el-tree>-->
								<el-tree
								  class="tree-border"
								  :data="options"
								  @check="checkStrictly"
								  show-checkbox
								  default-expand-all
								  node-key="id"
								  ref="tree"
								  :default-checked-keys="form.access" 
							
								  empty-text="加载中，请稍后"
								  highlight-current
								  :props="defaultProps">
								</el-tree>
						</el-form-item>
					</el-col>
				</el-row>
			</el-form>
			<div slot="footer" class="dialog-footer">
				<el-button :size="size" :loading="loading" type="primary" @click="submit" >
					<span v-if="!loading">确 定</span>
					<span v-else>提 交 中...</span>
				</el-button>
				<el-button :size="size" @click="closeForm">取 消</el-button>
			</div>
		</el-dialog>
	`
	,
	components:{
	},
	props: {
		show: {
			type: Boolean,
			default: false
		},
		size: {
			type: String,
			default: 'small'
		},
		info: {
			type: Object,
		},
	},
	data(){
		return {
			form: {
				title:'',
				access:[],
			},
			options:[],
			defaultProps: {
				children: "children",
				label: "module_name"
			},
			strictly:true,
			loading:false,
		}
	},
	watch: {
		show(value) {
			if(value){
				axios.post(base_url+'/admin/base/getRoleMenus').then(res => {
					if(res.data.status == 200){
						this.options = res.data.data
					}
				})
			}
		}
	},
	methods: {
		open(){
			this.form = this.info
			this.strictly = true
			this.setDefaultVal('access')
		},
		submit(){
			this.$refs['form'].validate(valid => {
				if(valid) {
					this.loading = true
					this.form.access = this.getMenuAllCheckedKeys()
					axios.post(base_url + '/admin/role/auth',this.form).then(res => {
						if(res.data.status == 200){
							this.$message({message: res.data.msg, type: 'success'})
							this.$emit('refesh_list')
							this.closeForm()
						}else{
							this.$message.error(res.data.msg)
						}
					}).catch(()=>{
						this.loading = false
					})
				}
			})
		},
		getMenuAllCheckedKeys() {
			let checkedKeys = this.$refs.tree.getCheckedKeys();
			// 半选中的部门节点
			let halfCheckedKeys = this.$refs.tree.getHalfCheckedKeys();
			checkedKeys.unshift.apply(checkedKeys, halfCheckedKeys);
			return checkedKeys
		},
		setDefaultVal(key){
			if(this.form[key] == null || this.form[key] == ''){
				this.form[key] = []
			}
		},
		checkStrictly(){
			this.strictly = false
		},
		closeForm(){
			this.$emit('update:show', false)
			this.loading = false
			if (this.$refs['form']!==undefined) {
				this.$refs['form'].resetFields()
			}
		},
	}
})
