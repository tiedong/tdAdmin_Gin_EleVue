Vue.component('Update', {
	template: `
		<div v-if="show">
			<el-form :size="size" ref="form" :model="form" :rules="rules" :label-width="'90px'">
			<el-tabs v-model="activeName">
				<el-row >
					<el-col :span="24">
						<el-form-item label="标题" prop="title">
							<el-input  v-model="form.title" autoComplete="off" clearable  placeholder="请输入标题"></el-input>
						</el-form-item>
					</el-col>
				</el-row>
				<el-row >
					<el-col :span="24">
						<el-form-item label="所属分类" prop="article_cate_id">
							<treeselect  v-if="show" :appendToBody="true" :default-expand-level="2" v-model="form.article_cate_id" :options="pids" :normalizer="normalizer" :show-count="true" zIndex="999999" placeholder="请选择所属父类"/>
						</el-form-item>
					</el-col>
				</el-row>
				<el-row >
					<el-col :span="24">
						<el-form-item label="封面图" prop="thumb">
							<Upload v-if="show" size="small" file_type="image"  :image.sync="form.thumb"></Upload>
						</el-form-item>
					</el-col>
				</el-row>
				<el-row >
					<el-col :span="24">
						<el-form-item label="关键词" prop="keywords">
							<el-input  v-model="form.keywords" autoComplete="off" clearable  placeholder="请输入关键词，用英文,分开"></el-input>
						</el-form-item>
					</el-col>
				</el-row>
				<el-row >
					<el-col :span="24">
						<el-form-item label="属性" prop="type">
							<el-select style="width:150px" v-model="form.type" filterable clearable placeholder="请选择">
								    <el-option
										  v-for="item in options"
										  :key="item.value"
										  :label="item.label"
										  :value="item.value">
										</el-option>
							</el-select>
						</el-form-item>
					</el-col>
				</el-row>
				<el-row >
					<el-col :span="24">
						<el-form-item label="状态" prop="status">
  							 <el-radio-group v-model="form.status">
								<el-radio :label="1">正常</el-radio>
								<el-radio :label="0">下架</el-radio>		
							  </el-radio-group>
						</el-form-item>
					</el-col>
				</el-row>
				<el-row >
					<el-col :span="24">
						<el-form-item label="首页显示" prop="is_home">
  							 <el-radio-group v-model="form.is_home">
								<el-radio :label="1">是</el-radio>
								<el-radio :label="0">否</el-radio>		
							  </el-radio-group>
						</el-form-item>
					</el-col>
				</el-row>
				<el-row >
					<el-col :span="24">
						<el-form-item label="是否原创" prop="original">
  							 <el-radio-group v-model="form.original">
								<el-radio :label="1">是</el-radio>
								<el-radio :label="0">否</el-radio>		
							  </el-radio-group>
						</el-form-item>
					</el-col>
				</el-row>
				<el-row >
					<el-col :span="24">
						<el-form-item label="作者/来源" prop="origin">
							<el-input  v-model="form.origin" autoComplete="off" clearable  placeholder="请输入作者/来源"></el-input>
						</el-form-item>
					</el-col>
				</el-row>
				<el-row >
					<el-col :span="24">
						<el-form-item label="来源链接" prop="origin_url">
							<el-input  v-model="form.origin_url" autoComplete="off" clearable  placeholder="请输入作者/来源"></el-input>
						</el-form-item>
					</el-col>
				</el-row>
				<el-row >
					<el-col :span="24">
						<el-form-item label="排序" prop="sort">
							<el-input-number controls-position="right" style="width:200px;" autoComplete="off" v-model="form.sort" clearable :min="0" placeholder="请输入排序"/>
						</el-form-item>
					</el-col>
				</el-row>
				<el-row>
                        <el-col :span="24">
                            <el-form-item label="文章摘要" prop="desc">
                                <el-input type="textarea" autoComplete="off" v-model="form.desc" 
                                          :autosize="{ minRows: 4, maxRows: 6}" clearable placeholder="请输入文章摘要,字数不要超过200字"/>
                            </el-form-item>
                        </el-col>
                    </el-row>
				<el-row >
					<el-col :span="24">
						<el-form-item label="内容详情" prop="content" v-if="show">
							<tinymce  :content.sync="form.content"></tinymce>
						</el-form-item>
					</el-col>
				</el-row>
			</el-tabs>
				<el-form-item>
					<el-button :size="size" type="primary" @click="submit">保存设置</el-button>
					<el-button :size="size" icon="el-icon-back" @click="closeForm">返回</el-button>
				</el-form-item>
			</el-form>
		</div>
	`
	,
	components:{
		'treeselect':VueTreeselect.Treeselect,
	},
	props: {
		show: {
			type: Boolean,
			default: false
		},
		size: {
			type: String,
			default: 'small'
		},
		info: {
			type: Object,
		},
	},
	data(){
		return {
			form: {
				title:'',
				status:1,
				is_home:1,
				original:1,
				content:'',
				type:null,
			},
			options: [
				{ label: '精华', value: 1 },
				{ label: '热门', value: 2 },
				{ label: '推荐', value: 3 }
			],
			pids:[],
			loading:false,
			activeName:'',
			rules: {
				title:[
					{required: true, message: '标题不能为空', trigger: 'blur'},
				],
				article_cate_id:[
					{required: true, message: '请选择分类', trigger: 'blur'},
				],
			}
		}
	},
	watch:{
		show(val){
			if(val){
				axios.post(base_url + '/admin/articlecate/getFieldList').then(res => {
					if(res.data.status == 200){
						this.pids = res.data.data.pids
					}
				})
			}
			if(val){
				this.open()
			}
		}
	},
	methods: {
		open(){
			this.form = this.info
			if(this.info.pid == '0' ){
				this.$delete(this.info,'pid')
			}
		},
		submit(){
			this.$refs['form'].validate(valid => {
				if(valid) {
					this.loading = true
					axios.post(base_url + '/admin/article/update',this.form).then(res => {
						if(res.data.status == 200){
							this.$message({message: res.data.msg, type: 'success'})
							this.$emit('refesh_list')
							this.closeForm()
						}else{
							this.loading = false
							this.$message.error(res.data.msg)
						}
					}).catch(()=>{
						this.loading = false
					})
				}
			})
		},
		setDefaultVal(key){
			if(this.form[key] == null || this.form[key] == ''){
				this.form[key] = []
			}
		},
		normalizer(node) {
			if (node.children && !node.children.length) {
				delete node.children
			}
			return {
				id: node.val,
				label: node.key,
				children: node.children
			}
		},
		closeForm(){
			this.$emit('update:show', false)
			this.loading = false
			if (this.$refs['form']!==undefined) {
				this.$refs['form'].resetFields()
			}
			this.$emit('changepage')
		},
	}
})
