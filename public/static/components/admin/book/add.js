Vue.component('Add', {
	template: `
		<el-dialog title="添加" width="650px" class="icon-dialog" :visible.sync="show" @open="open" :before-close="closeForm" append-to-body>
			<el-form :size="size" ref="form" :model="form" :rules="rules" :label-width=" ismobile()?'90px':'16%'">
				<el-row >
					<el-col :span="24">
						<el-form-item label="知识库名称" prop="title">
							<el-input  v-model="form.title" autoComplete="off" clearable  placeholder="请输入知识库名称"></el-input>
						</el-form-item>
					</el-col>
				</el-row>
				<el-row >
					<el-col :span="24">
						<el-form-item label="封面图" prop="thumb">
							<Upload v-if="show" size="small" file_type="image"  :image.sync="form.thumb"></Upload>
						</el-form-item>
					</el-col>
				</el-row>
				<el-row >
					<el-col :span="24">
						<el-form-item label="状态" prop="status">
  							 <el-radio-group v-model="form.status">
								<el-radio :label="1">正常</el-radio>
								<el-radio :label="0">下架</el-radio>		
							  </el-radio-group>
						</el-form-item>
					</el-col>
				</el-row>
				<el-row >
					<el-col :span="24">
						<el-form-item label="首页显示" prop="is_home">
  							 <el-radio-group v-model="form.is_home">
								<el-radio :label="1">是</el-radio>
								<el-radio :label="0">否</el-radio>		
							  </el-radio-group>
						</el-form-item>
					</el-col>
				</el-row>
				<el-row >
					<el-col :span="24">
						<el-form-item label="排序" prop="sort">
							<el-input-number controls-position="right" style="width:200px;" autoComplete="off" v-model="form.sort" clearable :min="0" placeholder="请输入排序"/>
						</el-form-item>
					</el-col>
				</el-row>
				<el-row >
						<el-col :span="24">
							<el-form-item label="知识库描述" prop="desc">
								 <el-input type="textarea" autoComplete="off" v-model="form.desc"
                                          :autosize="{ minRows: 4, maxRows: 6}" clearable placeholder="请输入知识库描述"/>
							</el-form-item>
						</el-col>
				</el-row>
			</el-form>
			<div slot="footer" class="dialog-footer">
				<el-button :size="size" :loading="loading" type="primary" @click="submit" >
					<span v-if="!loading">确 定</span>
					<span v-else>提 交 中...</span>
				</el-button>
				<el-button :size="size" @click="closeForm">取 消</el-button>
			</div>
		</el-dialog>
	`
	,
	components:{
	},
	props: {
		show: {
			type: Boolean,
			default: false
		},
		size: {
			type: String,
			default: 'small'
		},
	},
	data(){
		return {
			form: {
				title:'',
				is_home:0,
				status:1,
				desc:'',
			},
			role_ids:[],
			loading:false,
			rules: {
				title:[
					{required: true, message: '知识库名称不能为空', trigger: 'blur'},
				],
			}
		}
	},
	watch:{
		show(val){
			if(val){
			}
		}
	},
	methods: {
		open(){
		},
		submit(){
			this.$refs['form'].validate(valid => {
				if(valid) {
					this.loading = true
					axios.post(base_url + '/admin/book/add',this.form).then(res => {
						if(res.data.status == 200){
							this.$message({message: res.data.msg, type: 'success'})
							this.$emit('refesh_list')
							this.closeForm()
						}else{
							this.loading = false
							this.$message.error(res.data.msg)
						}
					}).catch(()=>{
						this.loading = false
					})
				}
			})
		},
		closeForm(){
			this.$emit('update:show', false)
			this.loading = false
			if (this.$refs['form']!==undefined) {
				this.$refs['form'].resetFields()
			}
		},
	}
})
