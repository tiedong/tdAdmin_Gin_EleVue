/*
 Navicat Premium Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 80012
 Source Host           : localhost:3306
 Source Schema         : vuebasic

 Target Server Type    : MySQL
 Target Server Version : 80012
 File Encoding         : 65001

 Date: 11/03/2024 22:09:11
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for td_access
-- ----------------------------
DROP TABLE IF EXISTS `td_access`;
CREATE TABLE `td_access`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0',
  `type` tinyint(1) NULL DEFAULT NULL,
  `action_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `module_id` int(11) NULL DEFAULT NULL,
  `sort` int(11) NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `add_time` int(11) NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  `icon` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '',
  `update_time` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 132 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of td_access
-- ----------------------------
INSERT INTO `td_access` VALUES (52, '权限管理', 1, '', '/admin/manager/getlist', 0, 105, '', 0, 1, 'layui-icon-vercode', 1708949098);
INSERT INTO `td_access` VALUES (54, '用户管理', 1, '', '/admin/manager/index', 52, 100, '管理员列表', 0, 1, '', 1709090261);
INSERT INTO `td_access` VALUES (55, '增加', 3, '增加管理员', '/admin/manager/add', 54, 101, '管理员列表', 0, 1, '', 0);
INSERT INTO `td_access` VALUES (56, '编辑', 3, '编辑管理员', '/admin/manager/edit', 54, 100, '编辑管理员', 0, 1, '', 0);
INSERT INTO `td_access` VALUES (57, '删除', 3, '删除管理员', '/admin/manager/del', 54, 100, '删除管理员', 0, 1, '', 0);
INSERT INTO `td_access` VALUES (115, '角色管理', 1, '', '/admin/role/index', 52, 106, '角色管理', 1708934285, 1, 'layui-icon-right', 1709090271);
INSERT INTO `td_access` VALUES (116, '添加', 3, '', '/admin/role/add', 115, 1, '', 1708935024, 1, '', 1708935024);
INSERT INTO `td_access` VALUES (117, '获取列表数据', 2, '', '/admin/role/getlist', 115, 2, '', 1708935080, 1, '', 1709090465);
INSERT INTO `td_access` VALUES (118, '获取列表数据', 2, '', '/admin/manager/getlist', 54, 1, '', 1708935727, 1, 'layui-icon-circle', 1708949699);
INSERT INTO `td_access` VALUES (119, '修改状态', 3, '', '/admin/manager/status', 54, 2, '', 1708935807, 1, 'layui-icon-rate-solid', 1708949676);
INSERT INTO `td_access` VALUES (120, '操作添加', 2, '', '/admin/manager/doadd', 54, 3, '', 1708935870, 1, '', 1708935870);
INSERT INTO `td_access` VALUES (121, '操作编辑', 2, '', '/admin/manager/doedit', 54, 4, '', 1708935896, 1, '', 1708935896);
INSERT INTO `td_access` VALUES (123, '菜单管理', 1, '', '/admin/access/index', 52, 108, '', 1709090239, 1, '', 1709090599);
INSERT INTO `td_access` VALUES (124, '添加', 3, '', '/admin/access/add', 123, 1, '', 1709090346, 1, '', 1709090346);
INSERT INTO `td_access` VALUES (125, '操作添加', 2, '', '/admin/access/doadd', 123, 2, '', 1709090374, 1, '', 1709090374);
INSERT INTO `td_access` VALUES (126, '编辑', 3, '', '/admin/access/edit', 123, 3, '', 1709090403, 1, '', 1709090403);
INSERT INTO `td_access` VALUES (127, '操作编辑', 2, '', '/admin/access/doedit', 123, 4, '', 1709090423, 1, '', 1709090423);
INSERT INTO `td_access` VALUES (128, '删除', 3, '', '/admin/access/del', 123, 5, '', 1709090444, 1, '', 1709090444);
INSERT INTO `td_access` VALUES (129, '获取列表数据', 2, '', '/admin/access/getlist', 123, 0, '', 1709090499, 1, '', 1709090499);
INSERT INTO `td_access` VALUES (130, '表格页面', 1, '', '/admin/test/index', 52, 109, '', 1709090637, 1, '', 1709090637);
INSERT INTO `td_access` VALUES (131, '系统设置', 1, '', '/index', 0, 109, '', 1709091183, 1, 'layui-icon-set', 1709091183);
INSERT INTO `td_access` VALUES (132, '基本配置', 1, '', '/admin/config/index', 131, 1, '', 1709131102, 1, '', 1709131102);

-- ----------------------------
-- Table structure for td_admin_user
-- ----------------------------
DROP TABLE IF EXISTS `td_admin_user`;
CREATE TABLE `td_admin_user`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `nickname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '超级管理员' COMMENT '昵称',
  `mobile` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '手机号码',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '邮箱',
  `qq` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'QQ',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态',
  `last_login_ip` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '最后登陆IP',
  `last_login_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '最后登陆时间',
  `add_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间',
  `role_id` int(10) NOT NULL DEFAULT 1 COMMENT '角色ID',
  `is_super` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否为超管（1是）',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `role_id`(`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '[系统] 管理用户' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of td_admin_user
-- ----------------------------
INSERT INTO `td_admin_user` VALUES (1, 'admin', '$2a$10$t2uyzUjBM08jNCPv0gT5bOIItrsy0s5oW0Ve0RUqSYU89QBj6qHdm\r\n', '超级管理员', '18819210073', 'DDD', 'DDDD', 1, 'DDDD', 0, 0, 0, 9, 1);

-- ----------------------------
-- Table structure for td_config
-- ----------------------------
DROP TABLE IF EXISTS `td_config`;
CREATE TABLE `td_config`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `data` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of td_config
-- ----------------------------
INSERT INTO `td_config` VALUES (1, 'site_description', '网站描述');
INSERT INTO `td_config` VALUES (2, 'auth_key', '38nfCIlkqancd');
INSERT INTO `td_config` VALUES (3, 'cache_status', '1');
INSERT INTO `td_config` VALUES (4, 'site_keyword', '网站关键字');
INSERT INTO `td_config` VALUES (5, 'site_url', 'www.swiftadmin.net');
INSERT INTO `td_config` VALUES (6, 'site_icp', '京ICP备13000001号');
INSERT INTO `td_config` VALUES (7, 'site_email', 'admin@swiftadmin.net');
INSERT INTO `td_config` VALUES (8, 'site_total', '');
INSERT INTO `td_config` VALUES (9, 'site_copyright', '版权信息：');
INSERT INTO `td_config` VALUES (10, 'site_status', '0');
INSERT INTO `td_config` VALUES (11, 'site_notice', '<p>您要访问的网站出现了问题！</p>');
INSERT INTO `td_config` VALUES (12, 'site_name', '基于PHP MYSQL的极速后台开发框架');

-- ----------------------------
-- Table structure for td_role
-- ----------------------------
DROP TABLE IF EXISTS `td_role`;
CREATE TABLE `td_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT NULL,
  `add_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of td_role
-- ----------------------------
INSERT INTO `td_role` VALUES (9, '超级管理员', '我是一个超级管理员', 1, 1631072961);
INSERT INTO `td_role` VALUES (14, '软件部门', '软件部门1', 1, 1631075350);
INSERT INTO `td_role` VALUES (16, '销售部门', '销售部门', 1, 1631589828);
INSERT INTO `td_role` VALUES (17, '测试部门', '测试部门', 1, 1709004259);

-- ----------------------------
-- Table structure for td_role_access
-- ----------------------------
DROP TABLE IF EXISTS `td_role_access`;
CREATE TABLE `td_role_access`  (
  `role_id` int(11) NOT NULL,
  `access_id` int(11) NOT NULL,
  INDEX `role_id`(`role_id`) USING BTREE,
  INDEX `access_id`(`access_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of td_role_access
-- ----------------------------
INSERT INTO `td_role_access` VALUES (14, 52);
INSERT INTO `td_role_access` VALUES (14, 54);
INSERT INTO `td_role_access` VALUES (14, 55);
INSERT INTO `td_role_access` VALUES (14, 56);
INSERT INTO `td_role_access` VALUES (14, 57);
INSERT INTO `td_role_access` VALUES (14, 76);
INSERT INTO `td_role_access` VALUES (14, 53);
INSERT INTO `td_role_access` VALUES (14, 59);
INSERT INTO `td_role_access` VALUES (14, 60);
INSERT INTO `td_role_access` VALUES (14, 61);
INSERT INTO `td_role_access` VALUES (14, 62);
INSERT INTO `td_role_access` VALUES (14, 78);
INSERT INTO `td_role_access` VALUES (14, 79);
INSERT INTO `td_role_access` VALUES (14, 80);
INSERT INTO `td_role_access` VALUES (14, 81);
INSERT INTO `td_role_access` VALUES (9, 52);
INSERT INTO `td_role_access` VALUES (9, 54);
INSERT INTO `td_role_access` VALUES (9, 55);
INSERT INTO `td_role_access` VALUES (9, 53);
INSERT INTO `td_role_access` VALUES (9, 59);
INSERT INTO `td_role_access` VALUES (9, 60);
INSERT INTO `td_role_access` VALUES (9, 61);
INSERT INTO `td_role_access` VALUES (9, 62);
INSERT INTO `td_role_access` VALUES (9, 63);
INSERT INTO `td_role_access` VALUES (9, 64);
INSERT INTO `td_role_access` VALUES (9, 67);
INSERT INTO `td_role_access` VALUES (9, 82);
INSERT INTO `td_role_access` VALUES (9, 83);
INSERT INTO `td_role_access` VALUES (9, 84);
INSERT INTO `td_role_access` VALUES (9, 85);
INSERT INTO `td_role_access` VALUES (9, 70);
INSERT INTO `td_role_access` VALUES (9, 71);
INSERT INTO `td_role_access` VALUES (16, 53);
INSERT INTO `td_role_access` VALUES (16, 59);
INSERT INTO `td_role_access` VALUES (16, 60);
INSERT INTO `td_role_access` VALUES (16, 61);
INSERT INTO `td_role_access` VALUES (16, 62);
INSERT INTO `td_role_access` VALUES (16, 78);
INSERT INTO `td_role_access` VALUES (16, 79);
INSERT INTO `td_role_access` VALUES (16, 80);
INSERT INTO `td_role_access` VALUES (16, 81);
INSERT INTO `td_role_access` VALUES (16, 63);
INSERT INTO `td_role_access` VALUES (16, 64);
INSERT INTO `td_role_access` VALUES (16, 67);
INSERT INTO `td_role_access` VALUES (16, 82);
INSERT INTO `td_role_access` VALUES (16, 83);
INSERT INTO `td_role_access` VALUES (16, 84);
INSERT INTO `td_role_access` VALUES (17, 52);
INSERT INTO `td_role_access` VALUES (17, 54);
INSERT INTO `td_role_access` VALUES (17, 118);
INSERT INTO `td_role_access` VALUES (17, 119);
INSERT INTO `td_role_access` VALUES (17, 120);
INSERT INTO `td_role_access` VALUES (17, 121);
INSERT INTO `td_role_access` VALUES (17, 56);
INSERT INTO `td_role_access` VALUES (17, 55);
INSERT INTO `td_role_access` VALUES (17, 115);
INSERT INTO `td_role_access` VALUES (17, 116);
INSERT INTO `td_role_access` VALUES (17, 117);

SET FOREIGN_KEY_CHECKS = 1;
