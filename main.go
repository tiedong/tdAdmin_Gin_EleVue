package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"os"
	_ "vueBasic/boot"
	"vueBasic/boot/initialize"
	"vueBasic/global"
)

func main() {
	gin.SetMode(global.CONF.System.RunMode) //设置环境
	if err := initialize.InitEnter(); err != nil {
		fmt.Println("启动失败：", err)
		os.Exit(1)
	}
}
