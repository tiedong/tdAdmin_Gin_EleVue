package config

type GeneralDB struct {
	Host    string `mapstructure:"host" yaml:"host"`
	Port    string `mapstructure:"port" yaml:"port"`
	User    string `mapstructure:"user" yaml:"user"`
	Pwd     string `mapstructure:"pwd" yaml:"pwd"`
	Name    string `mapstructure:"name" yaml:"name"`
	Prefix  string `mapstructure:"prefix" yaml:"prefix"`
	Charset string `mapstructure:"charset" yaml:"charset"`
}

type SpecializedDB struct {
	Type      string `mapstructure:"type" json:"type" yaml:"type"`
	AliasName string `mapstructure:"alias-name" json:"alias-name" yaml:"alias-name"`
	GeneralDB `yaml:",inline" mapstructure:",squash"`
	Disable   bool `mapstructure:"disable" json:"disable" yaml:"disable"`
}
