package config

// 上传文件配置
type Upload struct {
	UploadPath      string `mapstructure:"upload-path" yaml:"upload-path"`
	Disks           string `mapstructure:"disks" yaml:"disks"`
	OssType         string `mapstructure:"oss-type" yaml:"oss-type"`
	AccessPath      string `mapstructure:"access-path" yaml:"access-path"`
	CheckFileStatus bool   `mapstructure:"check-file-status" yaml:"check-file-status"`
}
