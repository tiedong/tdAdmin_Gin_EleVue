package config

type Cache struct {
	Driver string `mapstructure:"driver" yaml:"driver"`
	Redis
}

type Redis struct {
	Db       int    `mapstructure:"db" yaml:"db"`
	Addr     string `mapstructure:"addr" yaml:"addr"`
	Password string `mapstructure:"password" yaml:"password"`
}
