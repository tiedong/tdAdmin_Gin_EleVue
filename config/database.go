package config

type Database struct {
	Db `structs:"db"` //默认mysql链接请不要动 struces标签 是连接名称
}

type Db struct {
	GeneralDB `yaml:",inline" mapstructure:",squash"`
}

func (m Db) Dsn() string {
	return m.User + ":" + m.Pwd + "@tcp(" + m.Host + ":" + m.Port + ")/" + m.Name + "?" + m.Charset
}

//如果有其他类型的数据库，可以往下面定义结构体
