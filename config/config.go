package config

// 系统配置
type Config struct {
	System
	Database
	Captcha
	Cache
	Jwt
	Upload
	DbList []SpecializedDB `mapstructure:"db-list" json:"db-list" yaml:"db-list"`
}

type System struct {
	RunMode         string `mapstructure:"run-mode" yaml:"run-mode"`
	Port            int    `mapstructure:"port" yaml:"port"`
	AccessLogStatus bool   `mapstructure:"access-log-status" yaml:"access-log-status"`
	GlobalLimit     int    `mapstructure:"global-limit" yaml:"global-limit"`
}

type Jwt struct {
	AdminJwt
}
