package config

type AdminJwt struct {
	SigningKey  string `mapstructure:"signing-key" yaml:"signing-key"`
	ExpiresTime int64  `mapstructure:"expires-time" yaml:"expires-time"`
	Issuer      string `mapstructure:"issuer" yaml:"issuer"`
	Aud         string `mapstructure:"aud" yaml:"aud"`
}
