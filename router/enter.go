package router

import "vueBasic/router/admin"

// 获取后台路由
var AdminRouter = []interface{}{
	&admin.AdminLogin{},
	&admin.AdminBasic{},
	&admin.AdminRole{},
	&admin.AdminAdminuser{},
	&admin.AdminAccess{},
	&admin.AdminLog{},
	&admin.AdminArticleCate{},
	&admin.AdminArticle{},
	&admin.AdminBook{},
	&admin.AdminDoc{},
}
