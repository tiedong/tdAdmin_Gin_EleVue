package admin

import (
	"github.com/gin-gonic/gin"
	"vueBasic/app/admin/controller"
	"vueBasic/middleware"
)

type AdminLogin struct{}

func (*AdminLogin) Router(r *gin.RouterGroup) {
	r.GET("/admin/login/index", controller.LoginC.Index)                              //登录页面
	r.GET("/admin/login/verify", middleware.BeforeAction(), controller.LoginC.Verify) //验证码
	r.POST("/admin/login/dologin", controller.LoginC.DoLogin)
	r.GET("/admin/login/logout", controller.LoginC.Logout) //退出登录
}
