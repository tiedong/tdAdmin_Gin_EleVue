package admin

import (
	"github.com/gin-gonic/gin"
	"vueBasic/app/admin/controller"
	"vueBasic/middleware"
)

type AdminDoc struct{}

func (*AdminDoc) Router(r *gin.RouterGroup) {
	r.Use(middleware.JwtVerify("admin"), middleware.AuthMiddleware())
	{
		r.GET("admin/doc/index", controller.DocC.Index)                  //列表页面
		r.POST("admin/doc/list", controller.DocC.GetList)                //数据列表
		r.POST("admin/doc/add", controller.DocC.Add)                     //添加
		r.POST("admin/doc/getUpdateInfo", controller.DocC.GetUpdateInfo) //获取详情
		r.POST("admin/doc/update", controller.DocC.Update)               //更新
		r.POST("admin/doc/updateExt", controller.DocC.UpdateExt)         //更新字段
		r.POST("admin/doc/delete", controller.DocC.Delete)               //删除
	}
}
