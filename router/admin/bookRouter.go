package admin

import (
	"github.com/gin-gonic/gin"
	"vueBasic/app/admin/controller"
	"vueBasic/middleware"
)

type AdminBook struct{}

func (*AdminBook) Router(r *gin.RouterGroup) {
	r.Use(middleware.JwtVerify("admin"), middleware.AuthMiddleware())
	{
		r.GET("admin/book/index", controller.BookC.Index)                  //列表页面
		r.POST("admin/book/list", controller.BookC.GetList)                //数据列表
		r.POST("admin/book/add", controller.BookC.Add)                     //添加
		r.POST("admin/book/getUpdateInfo", controller.BookC.GetUpdateInfo) //获取详情
		r.POST("admin/book/update", controller.BookC.Update)               //更新
		r.POST("admin/book/updateExt", controller.BookC.UpdateExt)         //更新字段
		r.POST("admin/book/delete", controller.BookC.Delete)               //删除
	}
}
