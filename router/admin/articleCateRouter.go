package admin

import (
	"github.com/gin-gonic/gin"
	"vueBasic/app/admin/controller"
	"vueBasic/middleware"
)

type AdminArticleCate struct{}

func (*AdminArticleCate) Router(r *gin.RouterGroup) {
	r.Use(middleware.JwtVerify("admin"), middleware.AuthMiddleware())
	{
		r.GET("admin/articlecate/index", controller.ArticleCateC.Index)                  //列表页面
		r.POST("admin/articlecate/list", controller.ArticleCateC.GetList)                //数据列表
		r.POST("admin/articlecate/add", controller.ArticleCateC.Add)                     //添加
		r.POST("admin/articlecate/getUpdateInfo", controller.ArticleCateC.GetUpdateInfo) //获取详情
		r.POST("admin/articlecate/update", controller.ArticleCateC.Update)               //更新
		r.POST("admin/articlecate/updateExt", controller.ArticleCateC.UpdateExt)         //更新字段
		r.POST("admin/articlecate/delete", controller.ArticleCateC.Delete)               //删除
		r.POST("admin/articlecate/getFieldList", controller.ArticleCateC.GetFieldList)
	}
}
