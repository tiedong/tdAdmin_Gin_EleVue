package admin

import (
	"github.com/gin-gonic/gin"
	"vueBasic/app/admin/controller"
	"vueBasic/middleware"
)

type AdminAdminuser struct{}

func (*AdminAdminuser) Router(r *gin.RouterGroup) {
	r.Use(middleware.JwtVerify("admin"), middleware.AuthMiddleware())
	{
		r.GET("admin/adminuser/index", controller.AdminUserC.Index)   //列表页面
		r.POST("admin/adminuser/list", controller.AdminUserC.GetList) //数据列表
		r.POST("admin/adminuser/getFieldList", controller.AdminUserC.GetFieldList)
		r.POST("admin/adminuser/add", controller.AdminUserC.Add)                     //添加
		r.POST("admin/adminuser/getUpdateInfo", controller.AdminUserC.GetUpdateInfo) //获取详情
		r.POST("admin/adminuser/update", controller.AdminUserC.Update)               //更新
		r.POST("admin/adminuser/delete", controller.AdminUserC.Delete)               //删除
		r.POST("admin/adminuser/resetPwd", controller.AdminUserC.ResetPwd)           //重置密码
		r.POST("admin/adminuser/updateExt", controller.AdminUserC.UpdateExt)         //更新状态
	}
}
