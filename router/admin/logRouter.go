package admin

import (
	"github.com/gin-gonic/gin"
	"vueBasic/app/admin/controller"
	"vueBasic/middleware"
)

type AdminLog struct{}

func (*AdminLog) Router(r *gin.RouterGroup) {
	r.Use(middleware.JwtVerify("admin"), middleware.AuthMiddleware())
	{
		r.GET("admin/log/index", controller.LogC.Index)    //列表页面
		r.POST("admin/log/list", controller.LogC.GetList)  //数据列表
		r.POST("admin/log/detail", controller.LogC.Detail) //详情
		r.POST("admin/log/delete", controller.LogC.Delete) //删除
		r.POST("admin/log/export", controller.LogC.Export) //导出
	}
}
