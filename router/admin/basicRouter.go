package admin

import (
	"github.com/gin-gonic/gin"
	"vueBasic/app/admin/controller"
	"vueBasic/middleware"
)

type AdminBasic struct{}

func (*AdminBasic) Router(r *gin.RouterGroup) {
	r.Use(middleware.JwtVerify("admin"), middleware.AuthMiddleware())
	{
		r.GET("admin/index/index", controller.IndexC.Index)               //后台页面
		r.GET("admin/index/main", controller.IndexC.Main)                 //后台首页
		r.POST("admin/base/getMenu", controller.BasicC.GetMenu)           //左边菜单栏
		r.POST("admin/base/getRoleMenus", controller.BasicC.GetRoleMenus) //角色菜单

		//基本配置
		r.GET("admin/baseconfig/index", middleware.BeforeAction(), controller.BasicConfigC.Index) //基本配置
		r.POST("admin/baseconfig/update", controller.BasicConfigC.Update)                         //基本配置-更新
		r.POST("admin/baseconfig/getInfo", controller.BasicConfigC.GetInfo)                       //基本配置-获取信息

		//上传
		r.POST("admin/base/upload", middleware.BeforeAction(), controller.BasicC.Upload)         //上传图片
		r.POST("admin/base/deleteFile", middleware.BeforeAction(), controller.BasicC.DeleteFile) //删除图片
	}
}
