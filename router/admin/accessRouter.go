package admin

import (
	"github.com/gin-gonic/gin"
	"vueBasic/app/admin/controller"
	"vueBasic/middleware"
)

type AdminAccess struct{}

func (*AdminAccess) Router(r *gin.RouterGroup) {
	r.Use(middleware.JwtVerify("admin"), middleware.AuthMiddleware())
	{
		r.GET("admin/access/index", controller.AccessC.Index)                  //列表页面
		r.POST("admin/access/list", controller.AccessC.GetList)                //数据列表
		r.POST("admin/access/add", controller.AccessC.Add)                     //添加
		r.POST("admin/access/getUpdateInfo", controller.AccessC.GetUpdateInfo) //获取详情
		r.POST("admin/access/update", controller.AccessC.Update)               //修改
		r.POST("admin/access/delete", controller.AccessC.Delete)               //删除
		r.POST("admin/access/updateExt", controller.AccessC.UpdateExt)         //更新状态
		r.POST("admin/access/detail", controller.AccessC.Detail)               //详情
		r.POST("admin/access/getFieldList", controller.AccessC.GetFieldList)
	}
}
