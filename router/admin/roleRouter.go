package admin

import (
	"github.com/gin-gonic/gin"
	"vueBasic/app/admin/controller"
	"vueBasic/middleware"
)

type AdminRole struct{}

func (*AdminRole) Router(r *gin.RouterGroup) {
	r.Use(middleware.JwtVerify("admin"), middleware.AuthMiddleware())
	{
		r.GET("admin/role/index", controller.RoleC.Index)                  //列表页面
		r.POST("admin/role/list", controller.RoleC.GetList)                //数据列表
		r.POST("admin/role/add", controller.RoleC.Add)                     //添加
		r.POST("admin/role/getUpdateInfo", controller.RoleC.GetUpdateInfo) //获取详情
		r.POST("admin/role/update", controller.RoleC.Update)               //修改
		r.POST("admin/role/delete", controller.RoleC.Delete)               //删除
		r.POST("admin/role/updateExt", controller.RoleC.UpdateExt)         //更新状态
		r.POST("admin/role/auth", controller.RoleC.Auth)                   //授权
	}
}
