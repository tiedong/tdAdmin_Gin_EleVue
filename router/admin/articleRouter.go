package admin

import (
	"github.com/gin-gonic/gin"
	"vueBasic/app/admin/controller"
	"vueBasic/middleware"
)

type AdminArticle struct{}

func (*AdminArticle) Router(r *gin.RouterGroup) {
	r.Use(middleware.JwtVerify("admin"), middleware.AuthMiddleware())
	{
		r.GET("admin/article/index", controller.ArticleC.Index)                  //列表页面
		r.POST("admin/article/list", controller.ArticleC.GetList)                //数据列表
		r.POST("admin/article/add", controller.ArticleC.Add)                     //添加
		r.POST("admin/article/getUpdateInfo", controller.ArticleC.GetUpdateInfo) //获取详情
		r.POST("admin/article/update", controller.ArticleC.Update)               //更新
		r.POST("admin/article/updateExt", controller.ArticleC.UpdateExt)         //更新字段
		r.POST("admin/article/delete", controller.ArticleC.Delete)               //删除
	}
}
